import Currencies from "../Constants/currencies.json";

export const convertToCurrency = (code, val) => {
  // console.log("convertToCurrency HELPER : ", code);
  let ret = new Number(val).toLocaleString("en-US", {
    style: "currency",
    currency: code,
    minimumFractionDigits: 0,
  });
  // console.log("convertToCurrency HELPER : ", ret, " ", typeof ret);
  let fetchedCode = ret.substring(0, code.length);
  // console.log("convertToCurrency HELPER fetchedCode : ", fetchedCode);
  if (fetchedCode === code) {
    ret =
      Currencies[fetchedCode].symbol_native +
      ret.substring(code.length, ret.length);
  }
  return ret;
};

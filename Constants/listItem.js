export const incomeList = ["Business", "Loan", "Salary"];
export const expenseList = [
  "Clothing",
  "Drinks",
  "Education",
  "Food",
  "Fuel",
  "Fun",
  "Hospital",
  "Hotel",
  "Medical",
  "Merchandise",
  "Movie",
  "Other",
  "Personal",
  "Pets",
  "Restaurant",
  "Shopping",
  "Tips",
  "Transport",
];
export const paymentList = ["Cash", "Debit Card", "Credit Card", "Net Banking"];
export const EXPENSE_MODE = "expense-mode";
export const INCOME_MODE = "income-mode";
export const PAYMENT_MODE = "payment-mode";

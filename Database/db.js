import * as SQLite from "expo-sqlite";

const db = SQLite.openDatabase("my-pocket.db");

export const getAllTables = () => {
  return new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        "SELECT name FROM sqlite_master WHERE type ='table' AND name NOT LIKE 'sqlite_%';",
        // "DROP TABLE IF EXISTS credit_table;",
        [],
        (_, result) => {
          resolve(result);
        },
        (_, err) => {
          reject(err);
        }
      );
    });
  });
};

export const initDb = () => {
  const query1 =
    `CREATE TABLE IF NOT EXISTS transaction_table ( ` +
    `id integer primary key not null, ` +
    `amount REAL NOT NULL, ` +
    `description CHAR(50) NOT NULL, ` +
    // `expensed_in CHAR(50), ` +
    `transactionType CHAR(10) NOT NULL, ` +
    `date INT NOT NULL, ` +
    `moneyFor CHAR(20) NOT NULL, ` +
    `paymentMode Char(20) NOT NULL, ` +
    `forBorrowedItem BOOLEAN NOT NULL, ` +
    `status BOOLEAN NOT NULL); `;
  const query2 =
    `CREATE TABLE IF NOT EXISTS regular_items (` +
    `id integer primary key not null, ` +
    `description CHAR(50) not null, ` +
    `quantity integer not null, ` +
    `paid boolean not null, ` +
    `amount REAL NOT NULL, ` +
    `paymentMode Char(20) NOT NULL, ` +
    `date INT NOT NULL, ` +
    `transactionId INT, ` +
    `status BOOLEAN NOT NULL); `;
  const query3 =
    `CREATE TABLE IF NOT EXISTS listData (` +
    `id integer primary key not null, ` +
    `item CHAR(50) not null, ` +
    `itemType CHAR(50) not null, ` +
    `status BOOLEAN NOT NULL);`;
  const query4 =
    `CREATE TABLE IF NOT EXISTS settings (` +
    `id integer primary key not null, ` +
    `setting CHAR(50) not null, ` +
    `value CHAR(50) not null, ` +
    `status BOOLEAN NOT NULL);`;
  //   console.log("query: ", query);
  const promise = new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        query1,
        [],
        () => {
          tx.executeSql(
            query2,
            [],
            () => {
              tx.executeSql(
                query3,
                [],
                () => {
                  tx.executeSql(
                    query4,
                    [],
                    () => {
                      resolve();
                    },
                    (_, err) => {
                      reject(err);
                    }
                  );
                },
                (_, err) => {
                  reject(err);
                }
              );
            },
            (_, err) => {
              reject(err);
            }
          );
        },
        (_, err) => {
          reject(err);
        }
      );
    });
  });
  return promise;
};
export const initSettings = () => {
  let query = `INSERT INTO settings (setting, value, status) VALUES ('currency', 'INR', '1'), ('app-pin', '', '1'), ('email', '', '1');`;

  return new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        query,
        [],
        (_, result) => {
          resolve(result);
        },
        (_, err) => {
          reject(err);
        }
      );
    });
  });
};

export const addSingleListData = (item, itemType) => {
  let query = `INSERT INTO listData (item, itemType, status) VALUES (?, ?, ?);`;

  return new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        query,
        [item, itemType, 1],
        (_, result) => {
          resolve(result);
        },
        (_, err) => {
          reject(err);
        }
      );
    });
  });
};

export const addMultipleListData = (itemList, itemType) => {
  let query = `INSERT INTO listData (item, itemType, status) VALUES `;
  itemList.forEach((element) => {
    query += `("${element}", "${itemType}", "1"), `;
  });
  query = query.substring(0, query.length - 2);
  query += `;`;

  // console.log("QUERYYYY : ", query);

  return new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        query,
        [],
        (_, result) => {
          resolve(result);
        },
        (_, err) => {
          reject(err);
        }
      );
    });
  });
};

export const getAllListData = () => {
  return new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        `SELECT * FROM listData WHERE status='1';`,
        [],
        (_, result) => {
          resolve(result);
        },
        (_, err) => {
          reject(err);
        }
      );
    });
  });
};

export const insertAmount = (
  amount,
  desc,
  type,
  date,
  moneyFor,
  paymentMode,
  forBorrowedItem = false
) => {
  let query = `INSERT INTO transaction_table (amount, description, transactionType, date, moneyFor, paymentMode, forBorrowedItem, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?);`;

  return new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        query,
        [amount, desc, type, date, moneyFor, paymentMode, forBorrowedItem, 1],
        (_, result) => {
          resolve(result);
        },
        (_, err) => {
          reject(err);
        }
      );
    });
  });
};

export const getAll = () => {
  return new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        `SELECT * FROM transaction_table WHERE status='1';`,
        [],
        (_, result) => {
          resolve(result);
        },
        (_, err) => {
          reject(err);
        }
      );
    });
  });
};

export const getTransactionById = (id) => {
  return new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        `SELECT * FROM transaction_table WHERE id='${id}' and status='1';`,
        [],
        (_, result) => {
          resolve(result);
        },
        (_, err) => {
          reject(err);
        }
      );
    });
  });
};

export const getTotalAmount = (type, month) => {
  return new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        `SELECT SUM(amount) as totalAmount FROM transaction_table WHERE transactionType='${type}' and date>='${month}' and status='1';`,
        [],
        (_, result) => {
          resolve(result);
        },
        (_, err) => {
          reject(err);
        }
      );
    });
  });
};

export const addListItemToTable = (item, itemType) => {
  let query = `INSERT INTO listData (item, itemType, status) VALUES (?, ?, ?);`;

  return new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        query,
        [item, itemType, 1],
        (_, result) => {
          resolve(result);
        },
        (_, err) => {
          reject(err);
        }
      );
    });
  });
};

export const insertBorrowedItem = (
  description,
  quantity,
  paid,
  amount,
  paymentMode,
  date,
  amountInsertId
) => {
  let query = `INSERT INTO regular_items (description, quantity, paid, amount, paymentMode, date, transactionId, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?);`;

  return new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        query,
        [
          description,
          quantity,
          paid,
          amount,
          paymentMode,
          date,
          amountInsertId,
          1,
        ],
        (_, result) => {
          resolve(result);
        },
        (_, err) => {
          reject(err);
        }
      );
    });
  });
};

export const getBorrowedItemsFromTable = () => {
  return new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        `SELECT * FROM regular_items WHERE status='1';`,
        [],
        (_, result) => {
          resolve(result);
        },
        (_, err) => {
          reject(err);
        }
      );
    });
  });
};

export const getBorrowedItemByDesc = (desc) => {
  return new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        `SELECT * FROM regular_items WHERE description='${desc}' and status='1';`,
        [],
        (_, result) => {
          resolve(result);
        },
        (_, err) => {
          reject(err);
        }
      );
    });
  });
};

export const updateBorrowedItemByDesc = (
  id,
  desc,
  quantity,
  paid,
  amount,
  paymentMode,
  date,
  transactionId,
  status = 1
) => {
  return new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        `UPDATE regular_items SET quantity='${quantity}', paid='${paid}', amount='${amount}', paymentMode='${paymentMode}', date='${date}', transactionId='${transactionId}', status='${status}' WHERE id='${id}' AND description='${desc}' AND status='1';`,
        [],
        (_, result) => {
          resolve(result);
        },
        (_, err) => {
          reject(err);
        }
      );
    });
  });
};

export const deleteTransactionFromTable = (id) => {
  return new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        `UPDATE transaction_table SET status='0' WHERE id='${id}' AND status='1';`,
        [],
        (_, result) => {
          resolve(result);
        },
        (_, err) => {
          reject(err);
        }
      );
    });
  });
};

export const getSettings = () => {
  return new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        `SELECT * FROM settings WHERE status='1';`,
        [],
        (_, result) => {
          resolve(result);
        },
        (_, err) => {
          reject(err);
        }
      );
    });
  });
};

export const updateSetting = (setting, value) => {
  return new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        `UPDATE settings SET value='${value}' WHERE setting='${setting}' AND status='1';`,
        [],
        (_, result) => {
          resolve(result);
        },
        (_, err) => {
          reject(err);
        }
      );
    });
  });
};

export const updateTransactionInTable = (
  id,
  amount,
  desc,
  type,
  date,
  moneyFor,
  paymentMode,
  forBorrowedItem = false
) => {
  return new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        `UPDATE transaction_table SET amount='${amount}', description='${desc}', transactionType='${type}', date='${date}', moneyFor='${moneyFor}', paymentMode='${paymentMode}', forBorrowedItem='${forBorrowedItem}' WHERE id='${id}' AND status='1';`,
        [],
        (_, result) => {
          resolve(result);
        },
        (_, err) => {
          reject(err);
        }
      );
    });
  });
};

export const getStatementBetweenDates = (startDate, endDate) => {
  let query = `SELECT * FROM transaction_table WHERE status='1';`;
  if (startDate && endDate) {
    query = `SELECT * FROM transaction_table WHERE date>='${startDate}' and date<='${endDate}' and status='1';`;
  }
  // console.log("QUQUQ: ", query);
  return new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        query,
        [],
        (_, result) => {
          resolve(result);
        },
        (_, err) => {
          reject(err);
        }
      );
    });
  });
};

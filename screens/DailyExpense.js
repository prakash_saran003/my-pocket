import React, { useState } from "react";
import { StyleSheet } from "react-native";
import { useDispatch } from "react-redux";
import Form from "../Components/Layout/Form";
import { addTransaction, deleteTransaction } from "../Store/actions";

const DailyExpense = (props) => {
  console.log("[DailyExpense] params : ", props.route.params);

  const editTransaction = props.route.params
    ? props.route.params.editTransaction
    : false;

  const [keyValue, setKeyValue] = useState(new Date().getTime().toString());

  const dispatch = useDispatch();

  const dataToAdd = (data) => {
    // console.log("DATATOT ADDD: : ", data);
    const timeinMilliseconds = +new Date(data.date).getTime();

    dispatch(
      addTransaction({
        date: timeinMilliseconds,
        amount: +data.amount,
        description: data.description,
        type: data.transactionType,
        moneyFor: data.moneyFor,
        paymentMode: data.paymentMode,
        editTransaction: editTransaction ? editTransaction : false,
        id: props.route.params ? props.route.params.id : null,
      })
    );
    console.log("[DailyExpense] dataToAdd IF IF");
    if (editTransaction) {
      props.navigation.goBack();
    } else {
      setKeyValue(new Date().getTime().toString());
    }
  };

  const onDeleteTransaction = (id) => {
    // console.log("onDeleteTransaction : ", id);
    dispatch(deleteTransaction(props.route.params));
    props.navigation.goBack();
  };

  return (
    <>
      <Form
        key={keyValue}
        onSubmitForm={dataToAdd}
        preFill={{
          ...props.route.params,
          itemName:
            props.route.params && props.route.params.description
              ? props.route.params.description
              : null,
        }}
        onDeleteTransaction={onDeleteTransaction}
      />
    </>
  );
};

export default DailyExpense;

const styles = StyleSheet.create({});

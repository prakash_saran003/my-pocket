import { useToast } from "native-base";
import React, { useEffect } from "react";
import { StyleSheet } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import Form from "../Components/Layout/Form";
import { SUCCESS } from "../Constants/constants";
import { addBorrowedItem, eventMessageShown } from "../Store/actions";

const AddNewBorrowedItem = ({ route, navigation }) => {
  // console.log("AddNewBorrowedItem ROUTE : ", route.params);
  // const eventMessage = useSelector((state) => state.eventMessage);

  // console.log("AddNewBorrowedItem eventMessage : ", eventMessage);

  const dispatch = useDispatch();

  // const toast = useToast();
  // const id = "test-toast";
  // useEffect(() => {
  //   // console.log();
  //   if (eventMessage && eventMessage.type != null && !toast.isActive(id)) {
  //     toast.show({
  //       id,
  //       title: eventMessage.heading,
  //       status: eventMessage.type.toLowerCase(),
  //       description: eventMessage.message,
  //       onCloseComplete: () => {
  //         // console.log("CLOSING TOAST!!");
  //         dispatch(eventMessageShown());
  //       },
  //     });
  //     if (eventMessage.type === SUCCESS) {
  //       // navigation.goBack();
  //     }
  //   }
  // }, [eventMessage]);

  const dataToAdd = (data) => {
    // console.log("dataToAdd : ", data);
    const timeinMilliseconds = +data.date.getTime();

    dispatch(
      addBorrowedItem({
        description: data.description,
        quantity: +data.quantity,
        paid: data.payingNow ? 1 : 0,
        amount: +data.amount,
        paymentMode: data.payingNow ? data.paymentMode : "",
        date: timeinMilliseconds,
        checkForDuplicate: route.params.checkForDuplicate,
        update: data.update ? true : false,
        id: route.params.id ? route.params.id : null,
        type: data.transactionType,
        transactionId: route.params.transactionId
          ? route.params.transactionId
          : null,
      })
    );
    navigation.goBack();
  };

  return (
    <Form borrowedForm={true} onSubmitForm={dataToAdd} preFill={route.params} />
  );
};

export default AddNewBorrowedItem;

const styles = StyleSheet.create({});

import {
  Box,
  HStack,
  Icon,
  Text,
  useTheme,
  useToast,
  VStack,
} from "native-base";
import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  TouchableNativeFeedback,
  TouchableOpacity,
} from "react-native";
import NumberPad from "../Components/Layout/NumberPad";
import { Entypo, Ionicons } from "@expo/vector-icons";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { pinEnteredCorrectFunc } from "../Store/actions";
import { sendGridEmail } from "react-native-sendgrid";
import {
  ERROR,
  FROMEMAIL,
  SENDGRIDAPIKEY,
  SUCCESS,
} from "../Constants/constants";

const LockScreen = () => {
  const [pin, setPin] = useState([]);
  const [pinText, setPinText] = useState("Enter PIN");
  const [mailSendSuccess, setMailSendSuccess] = useState(false);

  const appPin = useSelector((state) => state.appPin);

  // console.log("PIN : ", pin);
  // console.log("LockScreen appPin : ", appPin);

  const dispatch = useDispatch();

  const numPressHandler = (num) => {
    if (pin.length < 4) {
      setPin((prevState) => {
        return [...prevState.concat(num)];
      });
    }
  };

  useEffect(() => {
    console.log("PIN CHECK : ", pin.join(""));
    if (pin.length === 4 && pin.join("").trim() === appPin.toString().trim()) {
      console.log("PIN CORRECT  ");
      dispatch(pinEnteredCorrectFunc());
    }
  }, [pin, appPin]);

  const backPressHandler = () => {
    if (pin.length > 0) {
      setPin((prevState) => {
        return prevState.filter((el, idx) => idx + 1 !== pin.length);
      });
    }
  };

  const onForgotPinHandler = () => {
    // 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
    sendGridEmail(
      SENDGRIDAPIKEY,
      "rpapps25@gmail.com",
      "prakashchoudhary0141@gmail.com",
      "Reset PIN",
      "Your Pin is " + appPin
    )
      .then((res) => {
        console.log("sendGridEmail res : ", res);
        setMailSendSuccess({
          heading: "Please check your mail.",
          type: SUCCESS,
        });
      })
      .catch((err) => {
        console.log("sendGridEmail err : ", err);
        setMailSendSuccess({
          heading: "An Error Occurred.",
          type: ERROR,
        });
      });
    // console.log("sendRequest : ", sendRequest);
  };

  const toast = useToast();
  const id = "test-toast";
  useEffect(() => {
    // console.log();
    if (mailSendSuccess && !toast.isActive(id)) {
      toast.show({
        id,
        title: mailSendSuccess.heading,
        status: mailSendSuccess.type.toLowerCase(),
        description: "",
        onCloseComplete: () => {
          // console.log("CLOSING TOAST!!");
          setMailSendSuccess(false);
        },
      });
    }
  }, [mailSendSuccess]);

  return (
    <VStack
      // style={{ borderColor: "red", borderWidth: 1 }}
      // justifyContent="space-evenly"
      alignItems="center"
      height="100%"
      jus
    >
      <Box
        height="25%"
        //  style={{ borderColor: "green", borderWidth: 1 }}
      >
        LOGO
      </Box>
      <VStack
        height="55%"
        justifyContent="space-evenly"
        alignItems="center"
        // style={{ borderColor: "white", borderWidth: 1 }}
      >
        <Text p={3}>{pinText}</Text>
        <HStack height="25px">
          {pin.map((el, idx) => {
            return (
              <Icon
                key={"pin" + el + idx}
                // size="md"
                as={<Entypo name="dot-single" size={24} color="black" />}
              />
            );
          })}
        </HStack>
        <VStack>
          <HStack justifyContent="space-between">
            <NumberPad text={1} onPress={numPressHandler} />
            <NumberPad text={2} onPress={numPressHandler} />
            <NumberPad text={3} onPress={numPressHandler} />
          </HStack>
          <HStack justifyContent="space-between">
            <NumberPad text={4} onPress={numPressHandler} />
            <NumberPad text={5} onPress={numPressHandler} />
            <NumberPad text={6} onPress={numPressHandler} />
          </HStack>
          <HStack justifyContent="space-between">
            <NumberPad text={7} onPress={numPressHandler} />
            <NumberPad text={8} onPress={numPressHandler} />
            <NumberPad text={9} onPress={numPressHandler} />
          </HStack>
          <HStack
            justifyContent="space-between"
            // style={{ borderColor: "white", borderWidth: 1 }}
          >
            <Box
              //   style={{ borderColor: "white", borderWidth: 1 }}
              p={4}
              px={5}
              m={3}
              mx={8}
            >
              <Text></Text>
            </Box>
            <NumberPad text={0} onPress={numPressHandler} />
            {pin.length > 0 ? (
              <TouchableNativeFeedback
                background={TouchableNativeFeedback.Ripple("grey", true)}
                onPress={backPressHandler}
              >
                <Box
                  p={2}
                  m={3}
                  mx={7}
                  // style={{
                  //   borderColor: "white",
                  //   borderWidth: 1,
                  //   //   alignItems: "center",
                  // }}
                >
                  <Icon
                    //   style={{ borderColor: "white", borderWidth: 1 }}
                    size="md"
                    as={
                      <Ionicons
                        name="backspace-sharp"
                        size={20}
                        color="black"
                      />
                    }
                  />
                </Box>
              </TouchableNativeFeedback>
            ) : (
              <Box
                //   style={{ borderColor: "white", borderWidth: 1 }}
                p={4}
                px={5}
                m={3}
                mx={8}
              >
                <Text></Text>
              </Box>
            )}
          </HStack>
          <HStack justifyContent="center">
            <TouchableOpacity onPress={onForgotPinHandler}>
              <Text px={5} fontSize="sm">
                Forgot PIN?
              </Text>
            </TouchableOpacity>
          </HStack>
        </VStack>
      </VStack>
    </VStack>
  );
};

export default LockScreen;

const styles = StyleSheet.create({});

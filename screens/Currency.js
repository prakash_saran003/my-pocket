import {
  Box,
  FlatList,
  FormControl,
  Icon,
  Input,
  KeyboardAvoidingView,
} from "native-base";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Ionicons } from "@expo/vector-icons";
import Currencies from "../Constants/currencies.json";
import Country from "../Constants/country.json";
import { updateCurrency } from "../Store/actions";
import CustomPureComponent from "../Components/CustomPureComponent";

const Currency = () => {
  const [searchText, setSearchText] = useState("");

  const selectedCurrency = useSelector((state) => state.selectedCurrency);

  console.log("selectedCurrency : ", selectedCurrency);

  const currencyCode = Object.keys(Currencies);

  let Countries = Country.filter((cc) => {
    let idx = currencyCode.findIndex((code) => code === cc.currency_code);
    if (idx >= 0) {
      return true;
    }
    return false;
  });

  Countries.sort((a, b) => {
    if (a.country < b.country) return -1;
    if (a.country > b.country) return 1;
    return 0;
  });

  const selectedCountry = Countries.find(
    (c) => c.currency_code === selectedCurrency
  );
  //   console.log("Countries 1: ", Countries.length);

  Countries = Countries.filter((c) => {
    return c.currency_code !== selectedCurrency;
  });

  Countries.unshift(selectedCountry);

  const [countryys, setCountryys] = useState(Countries);

  const dispatch = useDispatch();

  return (
    <Box padding={2}>
      <FormControl p={2} my={2}>
        <KeyboardAvoidingView behavior="height" keyboardVerticalOffset={10}>
          <Input
            placeholder="Search"
            onChangeText={(value) => {
              setSearchText(value);
              setCountryys(
                Countries.filter((c) => {
                  let ret = false;
                  let toSearch = searchText.toLowerCase();
                  if (searchText.length === 0) return true;
                  ret = Currencies[c.currency_code].symbol
                    .toLowerCase()
                    .includes(toSearch);
                  if (!ret) {
                    ret = Currencies[c.currency_code].name
                      .toLowerCase()
                      .includes(toSearch);
                  }
                  if (!ret) {
                    ret = Currencies[c.currency_code].name_plural
                      .toLowerCase()
                      .includes(toSearch);
                  }
                  if (!ret) {
                    ret = c.country.toLowerCase().includes(toSearch);
                  }
                  return ret;
                })
              );
            }}
            value={searchText}
            InputLeftElement={
              <Icon
                size="sm"
                ml={2}
                size={5}
                color="gray.400"
                as={<Ionicons name="ios-search" />}
              />
            }
          />
        </KeyboardAvoidingView>
      </FormControl>
      <FlatList
        data={countryys}
        keyExtractor={(item) => item.country}
        renderItem={({ item }) => (
          <CustomPureComponent
            selectedCurrency={selectedCurrency}
            item={item}
            onUpdateCurrency={() => {
              dispatch(updateCurrency(item.currency_code));
            }}
          />
        )}
      />
    </Box>
  );
};

export default Currency;

import { Box, Fab, FlatList, HStack, Icon, Text, VStack } from "native-base";
import React, { useEffect } from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import { AntDesign } from "@expo/vector-icons";
import { useDispatch, useSelector } from "react-redux";
import { getBorrowedItems } from "../Store/actions";
import Card from "../Components/Layout/Card";
import { useIsFocused } from "@react-navigation/native";

const BorrowedItemsList = ({ navigation }) => {
  const borrowedItems = useSelector((state) => state.borrowedItems);

  // console.log("borrowedItems : ", borrowedItems);

  let listData = [];

  if (borrowedItems)
    Object.keys(borrowedItems).forEach((key) => {
      let unpaidQuantity = 0;

      borrowedItems[key].forEach((itm) => {
        if (itm.paid === 0) {
          unpaidQuantity += itm.quantity;
        }
      });

      listData.push({
        id: key,
        itemName: key,
        paid: unpaidQuantity === 0 ? true : false,
        quantity: unpaidQuantity,
      });
    });
  listData.sort((a, b) => {
    if (a.itemName > b.itemName) return 1;
    else if (a.itemName < b.itemName) return -1;
    else return 0;
  });

  // console.log("listData : ", listData);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getBorrowedItems());
  }, []);

  const isFocused = useIsFocused();

  return (
    <>
      <FlatList
        data={listData}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <Box mx={4} my={2}>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate("BorrowedItemList", {
                  itemName: item.itemName,
                });
              }}
            >
              <Card>
                <HStack
                  justifyContent="space-between"
                  // borderColor="white"
                  // borderWidth={1}
                  width="100%"
                >
                  <Text textAlign="center" my="auto">
                    {item.itemName}
                  </Text>
                  {item.paid ? (
                    <Text textAlign="center" my="auto">
                      Paid
                    </Text>
                  ) : (
                    <VStack>
                      <Text fontSize="sm" textAlign="center" my="auto">
                        Unpaid Quantity
                      </Text>
                      <Text fontSize="md" textAlign="center" my="auto">
                        {item.quantity}
                      </Text>
                    </VStack>
                  )}
                </HStack>
              </Card>
            </TouchableOpacity>
          </Box>
        )}
      />
      {isFocused ? (
        <Fab
          onPress={() => {
            navigation.navigate("AddNewBorrowedItem", {
              checkForDuplicate: true,
            });
          }}
          placement="bottom-right"
          icon={<Icon color="white" as={<AntDesign name="plus" />} size="md" />}
        />
      ) : null}
    </>
  );
};

export default BorrowedItemsList;

const styles = StyleSheet.create({});

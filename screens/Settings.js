import { Box, HStack, Text, VStack } from "native-base";
import React from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import { useSelector } from "react-redux";
import Constants from "expo-constants";
import Card from "../Components/Layout/Card";
import Currencies from "../Constants/currencies.json";

const Settings = ({ navigation }) => {
  const selectedCurrency = useSelector((state) => state.selectedCurrency);
  const appPin = useSelector((state) => state.appPin);
  // console.log(Currencies[selectedCurrency]);

  //   console.log("Constants : ", Constants);

  return (
    <Box>
      <VStack p={3}>
        <TouchableOpacity
          style={styles.card}
          onPress={() => {
            navigation.navigate("Currency");
          }}
        >
          <Card>
            <HStack
              justifyContent="space-between"
              width="100%"
              alignItems="center"
            >
              <Text>Currency</Text>
              <Text fontSize="sm" color="grey">
                {Currencies[selectedCurrency].name +
                  ` (${Currencies[selectedCurrency].symbol_native})`}
              </Text>
            </HStack>
          </Card>
        </TouchableOpacity>
        <TouchableOpacity style={styles.card}>
          <Card>
            <HStack justifyContent="space-between" width="100%">
              <Text>Database</Text>
            </HStack>
          </Card>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.card}
          onPress={() => {
            navigation.navigate("PasswordScreen");
          }}
        >
          <Card>
            <HStack
              justifyContent="space-between"
              width="100%"
              alignItems="center"
            >
              <Text>PIN Lock</Text>
              <Text fontSize="sm" color="grey">
                set PIN Lock
              </Text>
            </HStack>
          </Card>
        </TouchableOpacity>
        <View style={styles.card}>
          <Card>
            <HStack
              justifyContent="space-between"
              width="100%"
              alignItems="center"
            >
              <Text>App Version</Text>
              <Text fontSize="sm" color="grey">
                {`Version ${Constants.manifest.version}`}
              </Text>
            </HStack>
          </Card>
        </View>
      </VStack>
    </Box>
  );
};

export default Settings;

const styles = StyleSheet.create({
  card: {
    marginVertical: 5,
  },
});

import { Box, Fab, FlatList, HStack, Icon, Text } from "native-base";
import React from "react";
import { useSelector } from "react-redux";
import Card from "../Components/Layout/Card";
import { AntDesign } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native";
import { useIsFocused } from "@react-navigation/native";

const BorrowedItemList = (props) => {
  const itemName = props.route.params.itemName;
  // console.log("BorrowedItemList : ", itemName);

  const borrowedItemStatement = useSelector(
    (state) => state.borrowedItems[itemName]
  );
  // console.log("borrowedItemStatement : ", borrowedItemStatement);

  const isFocused = useIsFocused();

  return (
    <Box>
      <FlatList
        data={borrowedItemStatement}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate("AddNewBorrowedItem", {
                  itemName: itemName,
                  quantity: item.quantity,
                  paid: item.paid,
                  editing: true,
                  amount: item.amount,
                  id: item.id,
                  paymentMode: item.paymentMode,
                  transactionId: item.transactionId,
                });
              }}
            >
              <Box p={3}>
                <Card>
                  <HStack width="100%">
                    <Text width="33%" textAlign="center">
                      {new Date(item.date).toLocaleDateString()}
                    </Text>
                    <Text width="33%" textAlign="center">
                      {item.quantity}
                    </Text>
                    <Box width="33%" alignItems="center">
                      {item.paid ? "Paid" : "Unpaid"}
                    </Box>
                  </HStack>
                </Card>
              </Box>
            </TouchableOpacity>
          );
        }}
      />
      {isFocused ? (
        <Fab
          onPress={() => {
            props.navigation.navigate("AddNewBorrowedItem", {
              itemName: itemName,
              checkForDuplicate: false,
            });
          }}
          placement="bottom-right"
          icon={<Icon color="white" as={<AntDesign name="plus" />} size="md" />}
        />
      ) : null}
    </Box>
  );
};

export default BorrowedItemList;

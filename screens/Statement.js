import { Box, Button, FlatList, HStack, Text } from "native-base";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import DateFilter from "../Components/Layout/DateFilter";
import ListItem from "../Components/Layout/ListItem";
import { CREDIT } from "../Constants/constants";
import { getStatement } from "../Store/actions";

const Statement = ({ navigation }) => {
  let statement = useSelector((state) => state.statement);
  // console.log("STATEMEMT : ", statement);
  const [dateBetween, setDateBetween] = useState({
    startDate: null,
    endDate: null,
  });

  if (statement && statement.length > 0) {
    let balance = 0;
    statement = statement.map((st) => {
      if (st.transactionType === CREDIT) {
        balance += st.amount;
      } else {
        balance -= st.amount;
      }
      return {
        ...st,
        balance: balance,
      };
    });
  }
  // console.log("STATE<EMT : ", statement);

  const searchBetweenDates = (st, en, searchStart) => {
    if (searchStart) {
      setDateBetween({
        startDate: st,
        endDate: en,
      });
    } else {
      setDateBetween({
        startDate: null,
        endDate: null,
      });
    }
  };

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getStatement(dateBetween.startDate, dateBetween.endDate));
  }, [dateBetween]);

  return (
    <Box m={1} height="100%">
      <DateFilter onSearch={searchBetweenDates} />
      {statement && statement.length === 0 ? (
        <Box justifyContent="center" alignItems="center" height="100%">
          <Text>No Data Present!</Text>
          <Text>Please add some transactions.</Text>
        </Box>
      ) : (
        <FlatList
          data={statement}
          renderItem={({ item }) => (
            <ListItem item={item} navigation={navigation} />
          )}
          keyExtractor={(item) => item.id.toString()}
        />
      )}
    </Box>
  );
};

export default Statement;

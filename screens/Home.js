import React, { useCallback, useEffect, useState } from "react";
import { Box, HStack, Stack, Text, VStack } from "native-base";
import { StyleSheet, TouchableOpacity } from "react-native";
import { useSelector } from "react-redux";
import {
  VictoryContainer,
  VictoryLabel,
  VictoryPie,
  VictoryTooltip,
} from "victory-native";
import Card from "../Components/Layout/Card";
import Svg from "react-native-svg";
import { useFocusEffect } from "@react-navigation/native";
import { convertToCurrency } from "../Helper/helper";
import CreatePdf from "../Components/CreatePdf";
import { MONTH_NAMES } from "../Constants/constants";

export function Home({ navigation }) {
  let pocketBalance = useSelector((state) => state);
  // console.log("[Home] pocketBalance : ", pocketBalance);

  // const dispatch = useDispatch();

  const date = new Date();

  const [balance, setBalance] = useState(null);
  const [expense, setExpense] = useState(null);
  const [income, setIncome] = useState(null);
  const [currMonth, setCurrMonth] = useState(MONTH_NAMES[date.getMonth()]);
  const [currYear, setCurrYear] = useState(date.getFullYear());

  // console.log("currMonth : ", currMonth);
  // console.log("currYear : ", currYear);

  useFocusEffect(
    useCallback(() => {
      // console.log("[Home] useFocusEffect : useFocusEffect ===>>> ");
      setBalance(pocketBalance.balance);
      setExpense(pocketBalance.expense);
      setIncome(pocketBalance.income);
      setCurrMonth(pocketBalance.currMonth);
      setCurrYear(pocketBalance.currYear);
    }, [pocketBalance])
  );

  return (
    <Stack>
      <Box
        // borderWidth={1}
        // borderColor="red.400"
        justifyContent="center"
        alignItems="center"
        maxHeight="300px"
      >
        {balance > 0 || income > 0 || expense > 0 ? (
          <Svg viewBox="0 0 250 250">
            <VictoryPie
              width={250}
              height={250}
              innerRadius={55}
              colorScale={["green", "red", "orange"]}
              containerComponent={<VictoryContainer responsive={false} />}
              data={[
                {
                  y: balance,
                  label: "Balance",
                  // +
                  // String(
                  //   balance >= 0
                  //     ? convertToCurrency(Math.ceil(balance))
                  //     : balance
                  // ),
                },
                {
                  y: expense,
                  label: "Expense",
                  // +
                  // String(
                  //   expense >= 0
                  //     ? convertToCurrency(Math.ceil(expense))
                  //     : expense
                  // ),
                },
                // {
                //   y: income,
                //   label:
                //     "Income\n" +
                //     String(
                //       income >= 0
                //         ? convertToCurrency(Math.ceil(income))
                //         : income
                //     ),
                // },
              ]}
              animate={{
                duration: 500,
                delay: 0,
                // easing: "polyIn",
              }}
              renderInPortal={true}
              labelComponent={
                <VictoryTooltip
                  active
                  constrainToVisibleArea
                  renderInPortal={false}
                  flyoutPadding={{ top: 5, bottom: 5, left: 10, right: 10 }}
                />
              }
            />
            <VictoryLabel
              textAnchor="middle"
              style={{ fontSize: 20, fill: "white" }}
              x={126}
              y={130}
              text={currMonth + "\n" + currYear}
            />
          </Svg>
        ) : (
          <Svg viewBox="0 0 250 250">
            <VictoryPie
              width={250}
              height={250}
              innerRadius={55}
              colorScale={["cyan"]}
              containerComponent={<VictoryContainer responsive={false} />}
              data={[{ y: 1 }]}
              animate={{
                duration: 1000,
                delay: 0,
                easing: "polyIn",
              }}
              renderInPortal={true}
            />
            <VictoryLabel
              textAnchor="middle"
              style={{ fontSize: 20, fill: "white" }}
              x={126}
              y={130}
              text={currMonth + "\n" + currYear}
            />
          </Svg>
        )}
      </Box>
      <Box
        mb={10}
        px={2}
        //  borderWidth={2} borderColor="green.400"
      >
        <HStack justifyContent="space-between">
          <Text fontSize="md">
            {"Month Chart: " + currMonth + " " + currYear}
          </Text>
          <Text fontSize="md" style={{ color: "green" }}>
            {"Main Balance: " +
              (balance >= 0
                ? convertToCurrency(pocketBalance.selectedCurrency, balance)
                : balance)}
          </Text>
        </HStack>
        <HStack justifyContent="space-between">
          <Text fontSize="sm" style={{ color: "orange" }}>
            {"Income: " +
              (income >= 0
                ? convertToCurrency(pocketBalance.selectedCurrency, income)
                : income)}
          </Text>
          <Text fontSize="sm" style={{ color: "red" }}>
            {"Expense: " +
              (expense >= 0
                ? convertToCurrency(pocketBalance.selectedCurrency, expense)
                : expense)}
          </Text>
          <Text fontSize="sm" style={{ color: "green" }}>
            {"Balance: " +
              (balance >= 0
                ? convertToCurrency(pocketBalance.selectedCurrency, balance)
                : balance)}
          </Text>
        </HStack>
      </Box>
      <VStack>
        <HStack
          px={4}
          mb={5}
          // alignItems="center"
          // borderColor="white"
          // justifyContent="space-between"
          // maxHeight="100px"
          // borderWidth={2}
          // borderColor="red.400"
        >
          <TouchableOpacity
            style={{ ...styles.card, minWidth: "44%" }}
            onPress={() => {
              navigation.navigate("DailyExpense");
            }}
          >
            <Card>Add Transaction</Card>
          </TouchableOpacity>
          <TouchableOpacity
            style={{ ...styles.card, minWidth: "44%", marginLeft: "auto" }}
            onPress={() => {
              navigation.navigate("Statement");
            }}
          >
            <Card>All Transactions</Card>
          </TouchableOpacity>
        </HStack>
        <HStack px={4} maxHeight="100px">
          <TouchableOpacity
            style={{
              ...styles.card,
              minWidth: "44%",
            }}
            onPress={() => {
              navigation.navigate("BorrowedItems");
            }}
          >
            <Card>Borrowed Item</Card>
          </TouchableOpacity>
          <TouchableOpacity
            style={{ ...styles.card, minWidth: "44%", marginLeft: "auto" }}
            onPress={() => {
              navigation.navigate("Settings");
            }}
          >
            <Card>Settings</Card>
          </TouchableOpacity>
        </HStack>
        {/* <HStack justifyContent="space-around" m={5}>
          <TouchableOpacity
            style={{
              ...styles.card,
              minWidth: "44%",
            }}
            onPress={() => {
              CreatePdf();
            }}
          >
            <Card>PDF DEMO</Card>
          </TouchableOpacity>
        </HStack> */}
      </VStack>
    </Stack>

    // <Box bg={colorMode === "dark" ? "black" : "white"} pt={12}>
    //   <ScrollView contentContainerStyle={{ width: "100%" }}>
    //     <Heading p={3} mx={2}>
    //       NativeBase@3.0.0
    //     </Heading>
    //     <Divider opacity={colorMode == "dark" ? "0.4" : "1"} />
    //     <HStack alignItems="center" space={6} py={4} px={3} mx={2}>
    //       <Ionicons
    //         name="moon-sharp"
    //         size={24}
    //         color={colorMode == "dark" ? "white" : "black"}
    //       />
    //       <Text>Dark Mode</Text>
    //       <Switch
    //         ml="auto"
    //         onToggle={toggleColorMode}
    //         isChecked={colorMode === "dark"}
    //       />
    //     </HStack>
    //     <Divider opacity={colorMode == "dark" ? "0.4" : "1"} />
    //   </ScrollView>
    // </Box>
  );
}

const styles = StyleSheet.create({
  card: {
    // borderWidth: 1,
    // borderColor: "white",
    maxWidth: "50%",
  },
});

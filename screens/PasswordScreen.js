import {
  Button,
  HStack,
  ScrollView,
  useToast,
  VStack,
} from "native-base";
import React, { useEffect, useState } from "react";
import { StyleSheet } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import Card from "../Components/Layout/Card";
import CustomInput from "../Components/Layout/Input";
import PinInputBar from "../Components/PinInputBar";
import PinTmp from "../Components/PinTmp";
import { SUCCESS } from "../Constants/constants";
import { eventMessageShown, updatePin } from "../Store/actions";

function validateEmail(email) {
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

const PasswordScreen = ({ navigation }) => {
  const appPin = useSelector((state) => state.appPin);
  const eventMessage = useSelector((state) => state.eventMessage);
  console.log("appPin : ", appPin);

  const [email, setEmail] = useState("");
  const [pin, setPin] = useState("");
  const [confirmPin, setConfirmPin] = useState("");
  const [disableBtn, setDisableBtn] = useState(true);
  // const [fingerLock, setFingerLock] = useState(false);

  useEffect(() => {
    console.log("disableBtn : ", disableBtn);
    console.log("pin : ", pin);
    console.log("confirmPin : ", confirmPin);
    if (
      email !== "" &&
      validateEmail(email) &&
      pin &&
      confirmPin &&
      pin === confirmPin
    ) {
      setDisableBtn(false);
    } else {
      setDisableBtn(true);
    }
  }, [email, pin, confirmPin]);

  const toast = useToast();
  const id = "test-toast";
  useEffect(() => {
    if (eventMessage && eventMessage.type != null && !toast.isActive(id)) {
      toast.show({
        id,
        title: eventMessage.heading,
        status: eventMessage.type.toLowerCase(),
        description: eventMessage.message,
        onCloseComplete: () => {
          // console.log("CLOSING TOAST!!");
          dispatch(eventMessageShown());
        },
      });
      if (eventMessage.type === SUCCESS) {
        navigation.goBack();
      }
    }
  }, [eventMessage]);

  const dispatch = useDispatch();

  const addPinAndEmail = () => {
    console.log("EMAIL : ", email);
    console.log("PIN : ", pin);
    // console.log("fingerLock : ", fingerLock);

    dispatch(
      updatePin({
        email: email,
        pin: pin,
        // fingerLock: fingerLock,
      })
    );
  };

  return (
    <VStack m={5}>
      <ScrollView keyboardShouldPersistTaps={"always"}>
        <Card>
          <CustomInput
            placeholder="Email"
            errorMsg="Please enter email."
            inputValue={email}
            disabled={false}
            onChange={(value, isValid) => {
              setEmail(value);
            }}
          />

          {/* <PinInputBar
            label="PIN"
            getPin={(p) => {
              if (p) {
                setPin(p);
              } else {
                setPin("");
              }
            }}
          />
          <PinInputBar
            label="Confirm PIN"
            getPin={(p) => {
              if (p) {
                setConfirmPin(p);
              } else {
                setConfirmPin("");
              }
            }}
            error={
              pin &&
              pin.length === 4 &&
              confirmPin &&
              confirmPin.length === 4 &&
              pin !== confirmPin
            }
          /> */}

          <PinTmp
            getPin={(p) => {
              setPin(p);
              setConfirmPin(p);
            }}
          />

          {/* <HStack
            width="100%"
            // style={{ borderColor: "red", borderWidth: 1 }}
            justifyContent="space-between"
            alignItems="center"
            p={2}
            pr={4}
          >
            <Text>Fingerprint Lock</Text>
            <Switch
              defaultIsChecked={fingerLock}
              // isDisabled={disableBtn}
              isChecked={fingerLock}
              size="lg"
              offTrackColor="rose.200"
              onTrackColor="lime.200"
              onToggle={(t) => {
                // console.log("SWITCH : ", t);
                setFingerLock((prevState) => !prevState);
              }}
            />
          </HStack> */}

          <HStack justifyContent="space-evenly" width="100%">
            <Button
              disabled={disableBtn}
              onPress={addPinAndEmail}
              mt={5}
              colorScheme="blue"
            >
              Set PIN
            </Button>
          </HStack>
          {appPin && appPin.length === 4 && (
            <HStack justifyContent="space-evenly" width="100%">
              <Button
                onPress={() => {
                  dispatch(
                    updatePin({
                      email: "",
                      pin: "",
                      // fingerLock: false,
                    })
                  );
                }}
                mt={5}
                colorScheme="red"
              >
                Remove Current PIN
              </Button>
            </HStack>
          )}
        </Card>
      </ScrollView>
    </VStack>
  );
};

export default PasswordScreen;

const styles = StyleSheet.create({});

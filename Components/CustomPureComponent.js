import { HStack, VStack, Text, Icon } from "native-base";
import React, { PureComponent } from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import Card from "./Layout/Card";
import Currencies from "../Constants/currencies.json";

class CustomPureComponent extends PureComponent {
  render() {
    {
      return (
        <TouchableOpacity
          style={{ padding: 5 }}
          onPress={() => {
            this.props.onUpdateCurrency();
          }}
        >
          <Card>
            <HStack width="100%" justifyContent="space-between">
              <VStack>
                <Text>
                  {this.props.item.country +
                    ` (${
                      Currencies[[this.props.item.currency_code]].symbol_native
                    })`}
                </Text>
                <Text fontSize="sm" color="grey">
                  {Currencies[this.props.item.currency_code].name}
                </Text>
              </VStack>
              {this.props.selectedCurrency ===
                this.props.item.currency_code && (
                <Icon
                  size="md"
                  mr={2}
                  //   size={5}
                  color={"green.500"}
                  as={<Ionicons name="checkmark" />}
                />
              )}
            </HStack>
          </Card>
        </TouchableOpacity>
      );
    }
  }
}

export default CustomPureComponent;

const styles = StyleSheet.create({});

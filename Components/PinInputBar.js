import { FormControl, HStack, Icon } from "native-base";
import React, { useEffect, useRef, useState } from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import PinInput from "./Layout/PinInput";

const PinInputBar = (props) => {
  const [pin, setPin] = useState({
    pin1: "",
    pin2: "",
    pin3: "",
    pin4: "",
  });
  const [showPin, setShowPin] = useState(false);
  const [pinDisable, setPinDisable] = useState({
    pin1: false,
    pin2: true,
    pin3: true,
    pin4: true,
  });

  const firstPin = useRef();
  const secondPin = useRef();
  const thirdPin = useRef();
  const forthPin = useRef();

  useEffect(() => {
    // console.log("PIN  : ", pin);
    if (
      pin.pin1 !== "" &&
      pin.pin2 !== "" &&
      pin.pin3 !== "" &&
      pin.pin4 !== ""
    ) {
      props.getPin(pin.pin1 + pin.pin2 + pin.pin3 + pin.pin4);
    } else {
      props.getPin(false);
    }
  }, [pin]);

  return (
    <FormControl p={2} my={2} isInvalid={props.error}>
      <FormControl.Label>{props.label}</FormControl.Label>
      <HStack justifyContent="space-between">
        <PinInput
          ref={firstPin}
          showPin={showPin}
          pin={pin.pin1}
          setPin={async (p) => {
            await setPin((prevState) => ({ ...prevState, pin1: p }));
            await setPinDisable({
              pin1: true,
              pin2: false,
              pin3: true,
              pin4: true,
            });
            secondPin.current.focus();
          }}
          //   nextPin={secondPin}
          //   isDisabled={pin.pin1 || pin.pin1 === 0}
          isDisabled={pinDisable.pin1}
        />
        <PinInput
          ref={secondPin}
          showPin={showPin}
          pin={pin.pin2}
          setPin={async (p) => {
            await setPin((prevState) => ({ ...prevState, pin2: p }));
            await setPinDisable({
              pin1: true,
              pin2: true,
              pin3: false,
              pin4: true,
            });
            thirdPin.current.focus();
          }}
          prevPin={firstPin}
          //   nextPin={thirdPin}
          deletePrevPin={async () => {
            await setPin((prevState) => ({ ...prevState, pin1: "" }));
            await setPinDisable({
              pin1: false,
              pin2: true,
              pin3: true,
              pin4: true,
            });
            return "deleted";
          }}
          //   isDisabled={pin.pin2 || pin.pin2 === 0 || !(pin.pin1 !== "")}
          isDisabled={pinDisable.pin2}
        />
        <PinInput
          ref={thirdPin}
          showPin={showPin}
          pin={pin.pin3}
          setPin={async (p) => {
            await setPin((prevState) => ({ ...prevState, pin3: p }));
            await setPinDisable({
              pin1: true,
              pin2: true,
              pin3: true,
              pin4: false,
            });
            forthPin.current.focus();
          }}
          prevPin={secondPin}
          //   nextPin={forthPin}
          deletePrevPin={async () => {
            await setPin((prevState) => ({ ...prevState, pin2: "" }));
            await setPinDisable({
              pin1: true,
              pin2: false,
              pin3: true,
              pin4: true,
            });
            return "deleted";
          }}
          //   isDisabled={pin.pin3 || pin.pin3 === 0 || !(pin.pin2 !== "")}
          isDisabled={pinDisable.pin3}
        />
        <PinInput
          ref={forthPin}
          showPin={showPin}
          pin={pin.pin4}
          setPin={(p) => setPin((prevState) => ({ ...prevState, pin4: p }))}
          prevPin={thirdPin}
          deletePrevPin={async () => {
            await setPin((prevState) => ({ ...prevState, pin3: "" }));
            await setPinDisable({
              pin1: true,
              pin2: true,
              pin3: false,
              pin4: true,
            });
            return "deleted";
          }}
          //   isDisabled={!(pin.pin3 !== "")}
          isDisabled={pinDisable.pin4}
        />
        <TouchableOpacity
          style={{
            justifyContent: "center",
            // borderWidth: 1,
            // borderColor: "red",
            paddingHorizontal: 10,
          }}
          onPress={() => setShowPin((prevState) => !prevState)}
        >
          <Icon
            size="sm"
            as={
              !showPin ? (
                <Ionicons name="eye-outline" size={24} color="black" />
              ) : (
                <Ionicons name="eye-off-outline" size={24} color="black" />
              )
            }
          />
        </TouchableOpacity>
      </HStack>
      {/* {props.error && ( */}
      <FormControl.ErrorMessage>
        PIN and Confirm PIN are not same!
      </FormControl.ErrorMessage>
      {/* )} */}
    </FormControl>
  );
};

export default PinInputBar;

const styles = StyleSheet.create({});

import { NavigationContainer } from "@react-navigation/native";
import { Box, useColorModeValue, useToast, useToken } from "native-base";
import React, { useEffect } from "react";
import { StyleSheet } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { RootNavigator } from "../Navigation/rootNavigator";
import { eventMessageShown } from "../Store/actions";

const RootComponent = () => {
  const [lightBg, darkBg] = useToken(
    "colors",
    ["coolGray.50", "blueGray.800"],
    "blueGray.800"
  );
  const bgColor = useColorModeValue(lightBg, darkBg);

  const eventMessage = useSelector((state) => state.eventMessage);

  const dispatch = useDispatch();

  const toast = useToast();
  const id = "test-toast";
  useEffect(() => {
    if (eventMessage && eventMessage.type != null && !toast.isActive(id)) {
      toast.show({
        id,
        title: eventMessage.heading,
        status: eventMessage.type.toLowerCase(),
        description: eventMessage.message,
        onCloseComplete: () => {
          // console.log("CLOSING TOAST!!");
          dispatch(eventMessageShown());
        },
      });
    }
  }, [eventMessage, toast]);

  return (
    <NavigationContainer
      theme={{
        colors: { background: bgColor },
      }}
    >
      <Box
        flex={1}
        w="100%"
        _light={{
          bg: "coolGray.50",
        }}
        _dark={{
          bg: "blueGray.900",
        }}
        // bg={useColorModeValue('', 'blueGray.900')}
        _web={{
          overflowX: "hidden",
        }}
      >
        <RootNavigator />
      </Box>
    </NavigationContainer>
  );
};

export default RootComponent;

const styles = StyleSheet.create({});

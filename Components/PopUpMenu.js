import { Icon, Menu } from "native-base";
import React from "react";
import { Entypo } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native";
import CreatePdf from "./CreatePdf";
import { useDispatch, useSelector } from "react-redux";
import { fileSaved } from "../Store/actions";

const PopUpMenu = () => {
  const statement = useSelector((state) => state.statement);
  const selectedCurrency = useSelector((state) => state.selectedCurrency);
  const dispatch = useDispatch();
  // console.log("[PopUpMenu] statement : ", statement);
  return (
    <Menu
      trigger={(triggerProps) => {
        return (
          <TouchableOpacity {...triggerProps}>
            <Icon
              size="sm"
              mr={2}
              size={5}
              // color="gray.400"
              as={<Entypo name="dots-three-vertical" size={24} color="black" />}
            />
          </TouchableOpacity>
        );
      }}
      placement="left top"
      shouldOverlapWithTrigger={false}
    >
      {/* <Menu.Item
        onPress={() => {
          console.log("ICONSOS excel");
        }}
      >
        Export Excel
      </Menu.Item> */}
      <Menu.Item
        onPress={() => {
          console.log("ICONSOS pdf");
          if (statement.length > 0) {
            CreatePdf(statement, selectedCurrency).then((res) => {
              console.log("CREATE PDF : ", res);
              dispatch(fileSaved(res));
            });
          }
        }}
      >
        Download PDF
      </Menu.Item>
    </Menu>
  );
};

export default PopUpMenu;

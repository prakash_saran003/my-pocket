import React, { useState } from "react";
import { HStack, Icon, useTheme } from "native-base";
import SmoothPinCodeInput from "react-native-smooth-pincode-input";
import { TouchableOpacity } from "react-native";
import { Ionicons } from "@expo/vector-icons";

const PinTmp = (props) => {
  const [code, setCode] = useState("");
  const [showPin, setShowPin] = useState(false);

  const theme = useTheme();

  let fontSize = theme.fontSizes[theme.components.Input.defaultProps.size];

  return (
    <HStack justifyContent="space-evenly">
      <SmoothPinCodeInput
        cellSpacing={15}
        // placeholder="o"
        password={!showPin}
        cellStyle={{
          borderBottomWidth: 2,
          borderColor: "gray",
        }}
        cellStyleFocused={{
          borderColor: "cyan",
        }}
        textStyle={{
          color: "white",
          fontSize: fontSize,
        }}
        value={code}
        onTextChange={(code) => setCode(code)}
        onFulfill={(ff) => {
          console.log("FULLff : ", ff);
          console.log("FULL : ", code);
          props.getPin(ff);
        }}
      />
      <TouchableOpacity
        style={{
          justifyContent: "center",
          paddingHorizontal: 10,
        }}
        onPress={() => setShowPin((prevState) => !prevState)}
      >
        <Icon
          size="sm"
          as={
            !showPin ? (
              <Ionicons name="eye-outline" size={24} color="black" />
            ) : (
              <Ionicons name="eye-off-outline" size={24} color="black" />
            )
          }
        />
      </TouchableOpacity>
    </HStack>
  );
};

export default PinTmp;

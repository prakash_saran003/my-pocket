import React, { useEffect, useState } from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import DateTimePicker from "@react-native-community/datetimepicker";
import Card from "./Card";
import { HStack, Text, useTheme } from "native-base";

const CustomDatePicker = (props) => {
  // console.log("CustomDatePicker : ", props.id);

  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);
  const [mode, setMode] = useState();
  const { colors } = useTheme();

  const onChange = (event, selectedDate) => {
    // console.log("onChange : ", selectedDate);
    const currentDate = selectedDate || date;
    setDate(currentDate);
    setShow((prevState) => {
      setMode(undefined);
      return false;
    });
    props.onChange(currentDate.toString());
  };

  let hour = date.getHours() > 12 ? date.getHours() - 12 : date.getHours();

  let time =
    (hour < 10 ? "0" + hour : hour) +
    ":" +
    (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes()) +
    " " +
    (date.getHours() > 12 ? "PM" : "AM");

  // let currDate =
  //   (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) +
  //   "/" +
  //   (date.getMonth() < 9 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1) +
  //   "/" +
  //   date.getFullYear();
  // console.log("currDate : ", currDate);

  useEffect(() => {
    // console.log("useEffect CustomDatePicker : ", date);
    if (mode) {
      setShow(true);
    } else {
      setShow(false);
    }
  }, [mode]);

  const selectTimeHandler = () => {
    setMode("time");
  };
  const selectDateHandler = () => {
    setMode("date");
  };

  return (
    <>
      <HStack justifyContent="space-evenly" my={2}>
        {!props.noTime && (
          <TouchableOpacity
            onPress={selectTimeHandler}
            style={styles.cardOuter}
          >
            <Card borderWidth={1} borderColor={colors["slateGray"]["200"]}>
              <Text>{time}</Text>
            </Card>
          </TouchableOpacity>
        )}
        <TouchableOpacity
          onPress={selectDateHandler}
          style={{ ...styles.cardOuter, ...props.style }}
        >
          <Card
            borderWidth={1}
            borderColor={colors["slateGray"]["200"]}
            padding={props.padding}
          >
            <Text>
              {new Date(
                date.getFullYear(),
                date.getMonth(),
                date.getDate()
              ).toDateString()}
            </Text>
          </Card>
        </TouchableOpacity>
      </HStack>
      {show && (
        <DateTimePicker
          testID={props.id ? props.id : "dateTimePicker"}
          value={date}
          mode={mode}
          is24Hour={false}
          display="default"
          onChange={onChange}
        />
      )}
    </>
  );
};

export default CustomDatePicker;

const styles = StyleSheet.create({
  cardOuter: {
    height: 100,
    justifyContent: "center",
    marginHorizontal: 5,
  },
});

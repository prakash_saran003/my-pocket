import {
  Button,
  Checkbox,
  FormControl,
  HStack,
  Icon,
  Radio,
  ScrollView,
  Text,
  VStack,
} from "native-base";
import React, { useState } from "react";
import Card from "./Card";
import CustomDatePicker from "./DatePicker";
import CustomInput from "./Input";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import SelectWithAddNew from "./SelectWithAddNew";
import { useDispatch, useSelector } from "react-redux";
import {
  EXPENSE_MODE,
  INCOME_MODE,
  PAYMENT_MODE,
} from "../../Constants/listItem";
import { addListItem, ADD_LIST_ITEM } from "../../Store/actions";
import { Keyboard } from "react-native";
import { CREDIT, DEBIT } from "../../Constants/constants";

const Form = (props) => {
  const { expenseList, incomeList, paymentList } = useSelector(
    (state) => state.listData
  );

  const { preFill } = props;
  console.log("FORM preFill : ", preFill);

  const [formData, setFormData] = useState({
    amount:
      preFill &&
      preFill.amount &&
      (preFill.paid === 1 || preFill.editTransaction)
        ? preFill.amount.toString()
        : "",
    amountValid: false,
    description: preFill && preFill.itemName ? preFill.itemName : "",
    descValid: false,
    transactionType:
      preFill && preFill.transactionType ? preFill.transactionType : DEBIT,
    date: new Date(),
    payingNow: preFill ? (preFill.paid ? true : false) : false,
    quantity: preFill && preFill.quantity ? preFill.quantity : "",
    quantityValid: false,
    moneyFor:
      preFill && preFill.moneyFor
        ? preFill.moneyFor
        : expenseList.length > 0
        ? expenseList[0].item
        : null,
    paymentMode:
      preFill && preFill.paymentMode
        ? preFill.paymentMode
        : paymentList.length > 0
        ? paymentList[0].item
        : null,
  });

  const onSubmit = () => {
    props.onSubmitForm({ ...formData, update: preFill && preFill.editing });
    Keyboard.dismiss();
  };

  const dispatch = useDispatch();

  const addNewListItemHandler = (itemName, itemType) => {
    // console.log("itemType : ", itemType);
    dispatch(
      addListItem({
        type: ADD_LIST_ITEM,
        item: itemName,
        itemType: itemType,
      })
    );
    if (itemType === PAYMENT_MODE) {
      setFormData((prevState) => {
        return {
          ...prevState,
          paymentMode: itemName,
        };
      });
    } else {
      setFormData((prevState) => {
        return {
          ...prevState,
          moneyFor: itemName,
        };
      });
    }
  };

  return (
    <VStack m={5}>
      <ScrollView keyboardShouldPersistTaps={"handled"}>
        <Card>
          {props.borrowedForm || preFill.forBorrowedItem ? null : (
            <FormControl isRequired isInvalid p={2}>
              <Radio.Group
                defaultValue={formData.transactionType}
                name="exampleGroup"
                onChange={(nextValue) => {
                  setFormData((prevState) => {
                    return {
                      ...prevState,
                      transactionType: nextValue,
                    };
                  });
                  if (nextValue === DEBIT) {
                    // setExpenseOrIncomeList(expenseList);
                    setFormData((prevState) => {
                      return {
                        ...prevState,
                        moneyFor: expenseList[0].item,
                      };
                    });
                  } else {
                    // setExpenseOrIncomeList(incomeList);
                    setFormData((prevState) => {
                      return {
                        ...prevState,
                        moneyFor: incomeList[0].item,
                      };
                    });
                  }
                }}
              >
                <HStack
                  // borderWidth={1}
                  // borderColor="yellow.400"
                  width="100%"
                  justifyContent="space-evenly"
                >
                  <Radio
                    colorScheme="secondary"
                    value={DEBIT}
                    my={1}
                    aria-label={DEBIT}
                  >
                    Expense
                  </Radio>
                  <Radio
                    colorScheme="emerald"
                    value={CREDIT}
                    my={1}
                    aria-label={CREDIT}
                  >
                    Income
                  </Radio>
                </HStack>
              </Radio.Group>
            </FormControl>
          )}

          {props.borrowedForm && (
            <CustomInput
              keyboardType="decimal-pad"
              placeholder="Quantity"
              errorMsg="Please enter quantity"
              inputValue={formData.quantity.toString()}
              onChange={(value, isValid) =>
                setFormData((prevState) => {
                  return {
                    ...prevState,
                    quantity: value,
                    quantityValid: isValid,
                  };
                })
              }
            />
          )}
          {props.borrowedForm && (
            <FormControl px={2} m={2} isInvalid>
              <FormControl.Label>Paying Now?</FormControl.Label>
              <Checkbox.Group
                colorScheme="green"
                defaultValue={formData.payingNow ? [] : ["no"]}
                onChange={(values) => {
                  console.log("VALUES : : ", values);
                  let payingNow = false;
                  if (values.length === 0) {
                    payingNow = true;
                  }
                  setFormData((prevState) => {
                    return {
                      ...prevState,
                      payingNow: payingNow,
                    };
                  });
                }}
                alignItems="flex-start"
              >
                <Checkbox
                  value="no"
                  size="md"
                  // isChecked={!formData.payingNow}
                  // defaultIsChecked
                  aria-label="paying-later"
                  icon={<Icon as={<MaterialCommunityIcons name="check" />} />}
                >
                  No
                </Checkbox>
              </Checkbox.Group>
            </FormControl>
          )}

          {((props.borrowedForm && formData.payingNow) ||
            !props.borrowedForm) && (
            <CustomInput
              keyboardType="decimal-pad"
              placeholder="Amount"
              inputValue={formData.amount.toString()}
              errorMsg="Please enter amount"
              disabled={preFill.forBorrowedItem}
              onChange={(value, isValid) =>
                setFormData((prevState) => {
                  return {
                    ...prevState,
                    amount: value,
                    amountValid: isValid,
                  };
                })
              }
            />
          )}

          {props.borrowedForm || preFill.forBorrowedItem ? null : (
            <SelectWithAddNew
              checkedItem={
                formData.moneyFor
                // expenseOrIncomeList.length > 0
                //   ? expenseOrIncomeList[0].item
                //   : null
              }
              list={
                formData.transactionType === DEBIT ? expenseList : incomeList
              }
              type={
                formData.transactionType === DEBIT ? EXPENSE_MODE : INCOME_MODE
              }
              onValueChange={(itemValue) => {
                setFormData((prevState) => {
                  return {
                    ...prevState,
                    moneyFor: itemValue,
                  };
                });
              }}
              addNewListItem={addNewListItemHandler}
            />
          )}

          <CustomInput
            placeholder="Description"
            errorMsg="Please describe?"
            inputValue={formData.description.toString()}
            disabled={
              (!!preFill &&
                !preFill.editTransaction &&
                preFill.itemName &&
                preFill.itemName !== "") ||
              preFill.forBorrowedItem
            }
            onChange={(value, isValid) =>
              setFormData((prevState) => {
                return {
                  ...prevState,
                  description: value,
                  descValid: isValid,
                };
              })
            }
          />

          {((props.borrowedForm && formData.payingNow) ||
            !props.borrowedForm) && (
            <SelectWithAddNew
              checkedItem={formData.paymentMode}
              list={paymentList}
              type={PAYMENT_MODE}
              disabled={preFill.forBorrowedItem}
              onValueChange={(itemValue) => {
                setFormData((prevState) => {
                  return {
                    ...prevState,
                    paymentMode: itemValue,
                  };
                });
              }}
              addNewListItem={addNewListItemHandler}
            />
          )}

          <CustomDatePicker
            onChange={(selectedDate) => {
              setFormData((prevState) => {
                return {
                  ...prevState,
                  date: selectedDate,
                };
              });
            }}
          />

          {preFill.forBorrowedItem ? (
            <Text>Please edit it in borrowed items!</Text>
          ) : (
            <HStack justifyContent="space-evenly" width="100%">
              <Button
                isDisabled={
                  formData.description === "" ||
                  (props.borrowedForm &&
                    formData.payingNow &&
                    (!formData.amountValid ||
                      !formData.descValid ||
                      !formData.quantityValid ||
                      formData.amount === "" ||
                      formData.quantity === "")) ||
                  (props.borrowedForm &&
                    !formData.payingNow &&
                    (!formData.descValid ||
                      !formData.quantityValid ||
                      formData.quantity === "")) ||
                  (!props.borrowedForm &&
                    (!formData.amountValid ||
                      !formData.descValid ||
                      formData.amount === ""))
                }
                onPress={onSubmit}
                mt={5}
                colorScheme="cyan"
              >
                {preFill && (preFill.editing || preFill.editTransaction)
                  ? "Update"
                  : "Submit"}
              </Button>
              {preFill.editTransaction && (
                <Button
                  onPress={() => props.onDeleteTransaction(preFill.id)}
                  mt={5}
                  colorScheme="blue"
                >
                  Delete
                </Button>
              )}
            </HStack>
          )}
        </Card>
      </ScrollView>
    </VStack>
  );
};

export default Form;

import { Box, Text } from "native-base";
import React from "react";
import { TouchableNativeFeedback } from "react-native";

const NumberPad = (props) => {
  return (
    <TouchableNativeFeedback
      background={TouchableNativeFeedback.Ripple("grey", true)}
      onPress={() => props.onPress(props.text)}
    >
      <Box
        //   style={{ borderColor: "white", borderWidth: 1 }}
        p={4}
        m={3}
        mx={8}
        // backgroundColor="transparent"
        // borderRadius={5}
      >
        <Text>{props.text}</Text>
      </Box>
    </TouchableNativeFeedback>
  );
};

export default NumberPad;

import { Input } from "native-base";
import React, { forwardRef } from "react";
import { StyleSheet } from "react-native";

const PinInput = forwardRef((props, ref) => {
  const onPinChange = (text) => {
    // console.log("onPinChange : ", text);
    if (
      text.trim() !== "" &&
      Number.isInteger(+text.trim()) &&
      !(props.pin !== "")
    ) {
      //   console.log("onPinChange IF : ", text);
      props.setPin(text.trim());
      //   if (props.nextPin) {
      //     props.nextPin.current.focus();
      //   }
    }
  };
  const onKeyPressHandler = ({ nativeEvent }) => {
    // console.log("onKeyPress : ", nativeEvent);
    if (nativeEvent.key === "Backspace") {
      if (props.pin) {
        props.setPin("");
      } else if (props.prevPin) {
        props.deletePrevPin().then((res) => {
          console.log("RESSS : ", res);
          props.prevPin.current.focus();
        });
      }
    }
  };

  return (
    <Input
      //   onFocus={() => {
      //     console.log("ONFOCUS disabled: ", props.isDisabled);
      //     //   setPinTouched(true);
      //     //   focusFirstEmpty();
      //   }}
      ref={ref}
      m={0}
      variant="underlined"
      textAlign="center"
      placeholder="o"
      textAlignVertical="bottom"
      keyboardType="number-pad"
      type="password"
      secureTextEntry={!props.showPin}
      value={props.pin.toString()}
      onChangeText={onPinChange}
      onKeyPress={onKeyPressHandler}
      isDisabled={props.isDisabled}
      disabledInputStyle={{ background: "transparent" }}
      style={{
        backgroundColor: "transparent",
      }}
      caretHidden={true}
    />
  );
});

export default PinInput;

const styles = StyleSheet.create({});

import React from "react";
import { Box, Heading, useTheme, useColorModeValue } from "native-base";

function Card(props) {
  const { colors } = useTheme();
  const colorMode = useColorModeValue("light", "dark");

  return (
    <Box
      bg={colorMode === "dark" ? colors["blueGray"]["900"] : colors["darkText"]}
      shadow={9}
      rounded="lg"
      justifyContent="center"
      p={props.padding ? props.padding : 5}
      borderWidth={props.borderWidth ? props.borderWidth : 0}
      borderColor={props.borderColor ? props.borderColor : null}
      alignItems="center"
    >
      {props.children}
    </Box>
  );
}
{
  /* <Heading
  size={["md", "lg", "md"]}
  noOfLines={2}
  py={5}
  px={2}
  textAlign="center"
>
  {props.children}
</Heading>; */
}

export default Card;

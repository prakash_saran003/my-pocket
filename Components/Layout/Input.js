import { FormControl, Input, KeyboardAvoidingView } from "native-base";
import React, { useEffect, useState } from "react";
import { StyleSheet } from "react-native";

const CustomInput = (props) => {
  const [inputText, setInputText] = useState(props.inputValue);
  const [invalid, setInvalid] = useState(false);

  useEffect(() => {
    props.onChange(inputText, invalid === false);
  }, [inputText]);

  return (
    <FormControl p={2} my={2} isRequired isInvalid={invalid}>
      {/* <FormControl.Label>Amount</FormControl.Label> */}
      <KeyboardAvoidingView behavior="height" keyboardVerticalOffset={10}>
        <Input
          keyboardType={props.keyboardType ? props.keyboardType : "default"}
          placeholder={props.placeholder}
          onChangeText={(value) => {
            if (value.length > 0) setInvalid(false);
            else setInvalid(true);
            setInputText(value);
          }}
          value={inputText}
          onBlur={() => setInvalid(inputText === "")}
          isDisabled={props.disabled}
        />
      </KeyboardAvoidingView>
      <FormControl.ErrorMessage
        _text={{ fontSize: "xs", color: "error.500", fontWeight: 500 }}
      >
        {props.errorMsg}
      </FormControl.ErrorMessage>
    </FormControl>
  );
};

export default CustomInput;

const styles = StyleSheet.create({});

import { Button, HStack, Icon } from "native-base";
import React, { useEffect, useState } from "react";
import { Ionicons, Entypo } from "@expo/vector-icons";
import CustomDatePicker from "./DatePicker";
import { TouchableOpacity } from "react-native";

function sameDay(d1, d2) {
  return (
    d1.getFullYear() === d2.getFullYear() &&
    d1.getMonth() === d2.getMonth() &&
    d1.getDate() === d2.getDate()
  );
}

const DateFilter = (props) => {
  const [filterOn, setFilterOn] = useState(false);
  const [startDate, setStartDate] = useState(
    new Date(new Date().setUTCHours(0, 0, 0, 0))
  );
  const [endDate, setEndDate] = useState(new Date());

  const onCancel = () => {
    setFilterOn(false);
  };

  useEffect(() => {
    // console.log("USEEFFECT DateFilter endDate:", endDate);
    if (startDate && endDate) {
      props.onSearch(startDate.getTime(), endDate.getTime(), filterOn);
      //   props.onSearch(startDate, endDate);
    }
  }, [startDate, endDate, filterOn]);

  return (
    <HStack justifyContent="space-evenly" mt={3} mb={2}>
      {filterOn === true ? (
        <HStack
          //   borderColor={"red"}
          //   borderWidth={1}
          justifyContent="space-between"
          alignItems="center"
        >
          <CustomDatePicker
            id="start"
            style={{ margin: 0, padding: 0, height: 40 }}
            padding={3}
            noTime={true}
            onChange={(selectedDate) => {
              if (selectedDate) {
                setStartDate(
                  new Date(new Date(selectedDate).setUTCHours(0, 0, 0, 0))
                );
              }
            }}
          />
          <CustomDatePicker
            id="end"
            style={{ margin: 0, padding: 0, height: 40 }}
            padding={3}
            noTime={true}
            onChange={(selectedDate) => {
              //   console.log("selectedDate : ", selectedDate);
              if (selectedDate) {
                if (sameDay(new Date(selectedDate), new Date())) {
                  setEndDate(new Date(selectedDate));
                } else {
                  setEndDate(
                    new Date(
                      new Date(selectedDate).setUTCHours(23, 59, 59, 999)
                    )
                  );
                }
              }
            }}
          />
          <TouchableOpacity onPress={onCancel}>
            <Icon
              size="lg"
              as={<Entypo name="cross" size={24} color="black" />}
            />
          </TouchableOpacity>
        </HStack>
      ) : (
        <Button
          startIcon={
            <Icon
              size="sm"
              as={<Ionicons name="filter" size={24} color="black" />}
            />
          }
          mb={2}
          onPress={() => setFilterOn((prevState) => !prevState)}
        >
          Filter
        </Button>
      )}
    </HStack>
  );
};

export default DateFilter;

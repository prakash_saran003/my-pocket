import {
  Box,
  Button,
  CheckIcon,
  FormControl,
  Icon,
  Input,
  Select,
} from "native-base";
import React, { useState } from "react";
import { Ionicons, Entypo } from "@expo/vector-icons";
import { ScrollView } from "react-native";

const SelectWithAddNew = (props) => {
  // console.log("SLECT WITH NEW props : ", props);

  const [addingNew, setAddingNew] = useState(false);
  const [textInput, setTextInput] = useState("");
  const [added, setAdded] = useState(false);

  const addToListHandler = () => {
    console.log("TICK PRESSEDDDDD");
    if (textInput.length > 0) {
      props.addNewListItem(textInput, props.type);
      setAdded(true);
      setTimeout(() => {
        setTextInput("");
        setAddingNew(false);
        setAdded(false);
      }, 500);
    } else {
      setAddingNew(false);
      setAdded(false);
    }
  };

  return (
    <FormControl p={2} my={2}>
      <Select
        keyboardShouldPersistTaps={"handled"}
        selectedValue={props.checkedItem}
        minWidth={200}
        accessibilityLabel="Select your favorite programming language"
        // placeholder="Select your favorite programming language"
        onValueChange={(itemValue) => {
          props.onValueChange(itemValue);
          setAddingNew(false);
        }}
        _selectedItem={{
          bg: "teal.600",
          endIcon: <CheckIcon size={5} />,
        }}
        mt={1}
        isDisabled={props.disabled}
      >
        {/* <ScrollView keyboardShouldPersistTaps={"handled"}> */}
        {props.list
          ? props.list.map((item) => {
              return (
                <Select.Item
                  key={item.item}
                  label={item.item[0].toUpperCase() + item.item.slice(1)}
                  value={item.item}
                />
              );
            })
          : null}

        <Box mx={10} my={2} keyboardShouldPersistTaps={"handled"}>
          {addingNew ? (
            <Input
              keyboardShouldPersistTaps={"handled"}
              autoFocus
              value={textInput}
              onChangeText={(text) => setTextInput(text)}
              InputRightElement={
                <Icon
                  size="md"
                  mr={2}
                  //   size={5}
                  color={textInput.length > 0 ? "green.500" : "gray.400"}
                  as={
                    textInput.length === 0 ? (
                      <Entypo name="cross" size={24} color="black" />
                    ) : added ? (
                      <Ionicons name="checkmark-done" />
                    ) : (
                      <Ionicons name="checkmark" />
                    )
                  }
                  onPress={addToListHandler}
                />
              }
            />
          ) : (
            <Button onPress={() => setAddingNew(true)}>Add New</Button>
          )}
        </Box>
        {/* </ScrollView> */}
      </Select>
    </FormControl>
  );
};

export default SelectWithAddNew;

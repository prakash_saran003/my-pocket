import { Box, HStack, Text } from "native-base";
import React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import { useSelector } from "react-redux";
import { CREDIT } from "../../Constants/constants";
import { convertToCurrency } from "../../Helper/helper";
import Card from "./Card";

const ListItem = (props) => {
  const selectedCurrency = useSelector((state) => state.selectedCurrency);
  // console.log("PROPS : ", props);
  const date = new Date(props.item.date).toDateString();
  return (
    <Box p={3}>
      <TouchableOpacity
        onPress={() => {
          props.navigation.navigate("DailyExpense", {
            ...props.item,
            editTransaction: true,
          });
        }}
      >
        <Card
          borderWidth={2}
          borderColor={
            props.item.transactionType === CREDIT ? "green.500" : "red.500"
          }
        >
          <HStack width="100%" justifyContent="space-between">
            <Text fontSize="md" textAlign="center">
              {date}
            </Text>
            <Text fontSize="md" textAlign="center">
              {props.item.description.length <= 15
                ? props.item.description
                : props.item.description.substring(0, 15) + "..."}
            </Text>
            <Text fontSize="md" textAlign="center">
              {convertToCurrency(selectedCurrency, props.item.amount)}
            </Text>
          </HStack>
        </Card>
      </TouchableOpacity>
    </Box>
  );
};

export default ListItem;

const styles = StyleSheet.create({});

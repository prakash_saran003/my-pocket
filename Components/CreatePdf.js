import { PermissionsAndroid } from "react-native";
import * as Print from "expo-print";
import * as RNFS from "react-native-fs";
import { APP_FOLDER_NAME, CREDIT, MONTH_NAMES } from "../Constants/constants";
import { convertToCurrency } from "../Helper/helper";

const requestReadPermission = async () => {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      {
        title: "Read Storage Permission",
        message: "Allow this app to read from your storage.",
        buttonNeutral: "Ask Me Later",
        buttonNegative: "Cancel",
        buttonPositive: "OK",
      }
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("read permission granted");
    } else {
      console.log("read permission denied");
    }
  } catch (err) {
    console.warn(err);
  }
};

const requestWritePermission = async () => {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      {
        title: "Write Storage Permission",
        message: "Allow this app to write to your storage.",
        buttonNeutral: "Ask Me Later",
        buttonNegative: "Cancel",
        buttonPositive: "OK",
      }
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("write permission granted");
    } else {
      console.log("write permission denied");
    }
  } catch (err) {
    console.warn(err);
  }
};

const createMainFolder = async () => {
  console.log("Creating Main Folder...");

  let dir = await RNFS.mkdir(
    RNFS.ExternalStorageDirectoryPath + "/" + APP_FOLDER_NAME
  );
  console.log("RNFS.mkdir : ", dir);
};

const saveFile = async (pdfURI) => {
  console.log("Saving File...");
  const d = new Date();
  let fileName =
    APP_FOLDER_NAME +
    "_Statement_" +
    d.toLocaleDateString().split("/").join("_") +
    "_" +
    d.toLocaleTimeString().split(":").join("_") +
    ".pdf";

  console.log("fileName : ", fileName);
  fileName =
    RNFS.ExternalStorageDirectoryPath + "/" + APP_FOLDER_NAME + "/" + fileName;
  console.log("fileName : ", fileName);

  await RNFS.moveFile(pdfURI, fileName);
  return fileName;
};

const getPermissions = async () => {
  //   await requestReadPermission();

  //   await requestWritePermission();
  //   return await PermissionsAndroid.requestMultiple([
  //     PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
  //     PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
  //   ]);
  return {
    read: await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE
    ),
    write: await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
    ),
  };
};

const getPDF = async (htmlContent) => {
  try {
    const { uri } = await Print.printToFileAsync({ html: htmlContent });
    return uri;
  } catch (err) {
    console.error(err);
  }
};

const getPDFContent = (statement, selectedCurrency) => {
  let totalIncome = 0,
    totalExpense = 0;

  let tmp = new Date(statement[0].date);

  const endDate =
    tmp.getDate() + " " + MONTH_NAMES[tmp.getMonth()] + " " + tmp.getFullYear();

  statement.reverse();
  tmp = new Date(statement[0].date);

  const startDate =
    tmp.getDate() + " " + MONTH_NAMES[tmp.getMonth()] + " " + tmp.getFullYear();

  let pdfContent = `
	<!DOCTYPE html>
	<html lang="en">
	
	<head>
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<title>My Pocket Statement</title>
			<style>
				body {
					font-size: 16px;
				}
		
				h1,
				h3 {
					text-align: center;
				}
		
				table,
				th,
				td {
					margin: auto;
					padding: 10px;
					text-align: center;
					border: 1px solid green;
				}

				td.debit, th.debit {
					border: 1px solid red;
				}

				th.heading {
					border: 2px solid blue;
				}

				table {
					border: 1px solid black;
					border-collapse: collapse;
					width: 90%;

				}
			</style>
		</head>
	
		<body>
			<h1>My Pocket Statement</h1>
			<h3>From ${startDate} to ${endDate}</h3>
			<table>
				<tbody>
					<tr class="heading">
						<th>Date & Time</th>
						<th>Mode</th>
						<th>Transaction Details</th>
						<th>Income</th>
						<th>Expense</th>
					</tr>`;

  statement.forEach((el) => {
    let date = new Date(el.date).toLocaleString();
    if (el.transactionType === CREDIT) {
      totalIncome += el.amount;
    } else {
      totalExpense += el.amount;
    }

    let className = el.transactionType === CREDIT ? "" : "debit";
    pdfContent += `<tr>
			<td class="${className}">${date}</td>
			<td class="${className}">${el.paymentMode}</td>
			<td class="${className}">${el.description}</td>
			<td class="${className}">${
      el.transactionType === CREDIT
        ? convertToCurrency(selectedCurrency, el.amount)
        : ""
    }</td>
			<td class="${className}">${
      el.transactionType === CREDIT
        ? ""
        : convertToCurrency(selectedCurrency, el.amount)
    }</td>
		</tr>`;
  });
  className = totalIncome >= totalExpense ? "" : "debit";
  pdfContent += `
				<tr>
					<th class="${className}"></th>
					<th class="${className}"></th>
					<th class="${className}">Total</th>
					<th class="${className}">${totalIncome}</th>
					<th class="${className}">${totalExpense}</th>
				</tr>
			</tbody>
		</table>
	</body>
	
	</html>
	`;
  return pdfContent;
};

const CreatePdf = async (statement, selectedCurrency) => {
  let pdfContent = getPDFContent(statement, selectedCurrency);
  let pdfURI = await getPDF(pdfContent);

  let permission = await getPermissions();
  console.log("permission : ", permission);
  if (permission.read === "denied" || permission.write === "denied") {
    return {
      success: false,
      heading: "Error!",
      message: "Permission to access storage is denied!",
    };
  }

  let mainFolderExists = await RNFS.exists(
    RNFS.ExternalStorageDirectoryPath + "/" + APP_FOLDER_NAME
  );

  console.log("mainFolderExists : ", mainFolderExists);
  let fileName;
  if (mainFolderExists) {
    fileName = await saveFile(pdfURI);
  } else {
    await createMainFolder();
    fileName = await saveFile(pdfURI);
  }
  console.log("fileName LAST : ", fileName);
  return {
    success: true,
    heading: "File Saved!",
    message: "Your file has been saved at this location - " + fileName,
  };
};

export default CreatePdf;

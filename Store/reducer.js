import clone from "just-clone";
import { CREDIT, MONTH_NAMES, SUCCESS } from "../Constants/constants";
import { EXPENSE_MODE, INCOME_MODE, PAYMENT_MODE } from "../Constants/listItem";
import { convertToCurrency } from "../Helper/helper";
import {
  ADD_BORROWED_ITEM,
  ADD_LIST_ITEM,
  ADD_TRANSACTION,
  DELETE_TRANSACTION,
  ERROR,
  ERROR_SHOWN,
  FILE_SAVED,
  GET_BORROWED_ITEM,
  GET_BORROWED_ITEMS,
  GET_STATEMENT,
  INIT,
  PIN_ENTERED,
  UPDATE_BORROWED_ITEM,
  UPDATE_CURRENCY,
  UPDATE_PIN,
  UPDATE_TRANSACTION,
} from "./actions";

const inititalState = {
  selectedCurrency: "INR",
  appPin: null,
  pinEnteredCorrect: false,
  // fingerLock: false,
  balance: 0,
  income: 0,
  expense: 0,
  currMonth: "",
  currYear: "",
  listData: {
    incomeList: [],
    expenseList: [],
    paymentList: [],
  },
  statement: [],
  borrowedItems: [],
  borrowedItemStatement: [],
  eventMessage: {
    type: null,
    heading: null,
    message: null,
  },
};

export default (state = inititalState, action) => {
  let updatedBorrowedItems;
  switch (action.type) {
    case INIT:
      const date = new Date();
      // console.log("INIT action : ", action);

      return {
        ...state,
        balance: action.balance,
        income: action.income,
        expense: action.expense,
        currMonth: MONTH_NAMES[date.getMonth()],
        currYear: date.getFullYear(),
        listData: {
          expenseList: action.listData.filter(
            (el) => el.itemType === EXPENSE_MODE
          ),
          incomeList: action.listData.filter(
            (el) => el.itemType === INCOME_MODE
          ),
          paymentList: action.listData.filter(
            (el) => el.itemType === PAYMENT_MODE
          ),
        },
        appPin: action.appPin,
        selectedCurrency: action.currency,
        pinEnteredCorrect: false,
        statement: [],
        borrowedItems: [],
        borrowedItemStatement: [],
        eventMessage: {
          type: null,
          heading: null,
          message: null,
        },
      };

    case ADD_LIST_ITEM:
      let updatedListData = {
        expenseList: [...state.listData.expenseList],
        incomeList: [...state.listData.incomeList],
        paymentList: [...state.listData.paymentList],
      };
      // console.log(
      //   "REDUCER updatedListData.incomeList 1 : ",
      //   updatedListData.incomeList
      // );

      if (action.itemType === INCOME_MODE) {
        updatedListData.incomeList.push({
          id: action.id,
          item: action.item,
          itemType: action.itemType,
        });
      }
      if (action.itemType === EXPENSE_MODE) {
        updatedListData.expenseList.push({
          id: action.id,
          item: action.item,
          itemType: action.itemType,
        });
      }
      if (action.itemType === PAYMENT_MODE) {
        updatedListData.paymentList.push({
          id: action.id,
          item: action.item,
          itemType: action.itemType,
        });
      }

      // console.log(
      //   "REDUCER updatedListData.incomeList 2 : ",
      //   updatedListData.incomeList
      // );

      return {
        ...state,
        listData: updatedListData,
      };

    case ADD_TRANSACTION:
      // console.log("ADD_TRANSACTION action : ", action);
      let expense = state.expense;
      let income = state.income;

      if (action.transactionType === CREDIT) {
        income += action.amount;
      } else {
        expense += action.amount;
      }

      return {
        ...state,
        income: income,
        expense: expense,
        balance: +income - +expense,
        eventMessage: {
          type: SUCCESS,
          heading: "Transaction Added!",
          message: `${convertToCurrency(
            state.selectedCurrency,
            action.amount
          )} is ${
            action.transactionType === CREDIT ? "credited to" : "debited from"
          } your main account.`,
        },
      };

    case UPDATE_TRANSACTION:
      // console.log("[reducer] UPDATE_TRANSACTION : ", action);
      expense = state.expense;
      income = state.income;

      if (action.data.transactionType === CREDIT) {
        income += action.data.amount;
      } else {
        expense += action.data.amount;
      }

      if (action.prevValues) {
        if (action.prevValues.transactionType === CREDIT) {
          income -= action.prevValues.amount;
        } else {
          expense -= action.prevValues.amount;
        }
      }

      let updatedStatement = clone(state.statement);

      let obbbbjjIdx = updatedStatement.findIndex(
        (el) => el.id === action.data.id
      );

      updatedStatement[obbbbjjIdx] = action.data;

      updatedStatement.sort((a, b) => {
        if (a.date > b.date) return -1;
        else if (a.date < b.date) return 1;
        else {
          if (a.id > b.id) return -1;
          else if (a.id < b.id) return 1;
          else return 0;
        }
      });

      return {
        ...state,
        income: income,
        expense: expense,
        balance: +income - +expense,
        statement: updatedStatement,
        eventMessage: {
          type: SUCCESS,
          heading: "Transaction Updated!",
          message: `${convertToCurrency(
            state.selectedCurrency,
            action.data.amount
          )} is ${
            action.data.transactionType === CREDIT
              ? "credited to"
              : "debited from"
          } your main account.`,
        },
      };

    case DELETE_TRANSACTION:
      let expense1 = state.expense;
      let income1 = state.income;

      if (action.data.transactionType === CREDIT) {
        income1 -= action.data.amount;
      } else {
        expense1 -= action.data.amount;
      }

      updatedStatement = state.statement.filter(
        (st) => st.id !== action.data.id
      );

      return {
        ...state,
        income: income1,
        expense: expense1,
        balance: +income1 - +expense1,
        eventMessage: {
          type: SUCCESS,
          heading: "Transaction Deleted!",
          message: null,
        },
        statement: updatedStatement,
      };

    case GET_STATEMENT:
      return {
        ...state,
        statement: action.data,
      };

    case ADD_BORROWED_ITEM:
      // console.log("[reducer] action.data : ", action.data);
      updatedBorrowedItems = clone(state.borrowedItems);
      if (action.data.description in updatedBorrowedItems) {
        updatedBorrowedItems[action.data.description].unshift(action.data);
      } else {
        updatedBorrowedItems[action.data.description] = [action.data];
      }

      expense = state.expense;
      income = state.income;

      if (action.data.paid) {
        if (action.data.transactionType === CREDIT) {
          income += action.data.amount;
        } else {
          expense += action.data.amount;
        }
      }

      return {
        ...state,
        income: income,
        expense: expense,
        balance: +income - +expense,
        borrowedItems: updatedBorrowedItems,
        eventMessage: {
          type: SUCCESS,
          heading: "Added Successfully!",
          message: null,
        },
      };

    case GET_BORROWED_ITEMS:
      return { ...state, borrowedItems: action.data };

    case GET_BORROWED_ITEM:
      return { ...state, borrowedItemStatement: action.data };

    case UPDATE_BORROWED_ITEM:
      // console.log("[reducer] action.data : ", action.data);
      updatedBorrowedItems = clone(state.borrowedItems);
      const idx = updatedBorrowedItems[action.data.description].findIndex(
        (el) => el.id === action.data.id
      );

      let obj = updatedBorrowedItems[action.data.description].find(
        (el) => el.id === action.data.id
      );

      let newObj = clone(obj);

      newObj.quantity = action.data.quantity;
      newObj.paid = action.data.paid;
      newObj.amount = action.data.amount;
      newObj.paymentMode = action.data.paymentMode;
      newObj.date = action.data.date;
      newObj.transactionId = action.data.transactionId;

      updatedBorrowedItems[action.data.description][idx] = newObj;

      expense = state.expense;
      income = state.income;

      if (action.data.paid) {
        if (action.data.transactionType === CREDIT) {
          income += action.data.amount;
        } else {
          expense += action.data.amount;
        }
      }
      if (action.data.expenseToAdd) {
        expense += action.data.expenseToAdd;
      }

      return {
        ...state,
        income: income,
        expense: expense,
        balance: +income - +expense,
        borrowedItems: updatedBorrowedItems,
        eventMessage: {
          type: SUCCESS,
          heading: "Updated Successfully!",
          message: null,
        },
      };

    case UPDATE_CURRENCY:
      return {
        ...state,
        selectedCurrency: action.code,
      };

    case UPDATE_PIN:
      // console.log("UPDATE_PIN : ", action);

      return {
        ...state,
        appPin: action.pin === "" ? null : action.pin,
        // fingerLock: action.fingerLock,
        eventMessage: {
          type: action.pin && action.pin.length === 4 ? SUCCESS : ERROR,
          heading:
            action.pin && action.pin.length === 4
              ? "App Lock PIN added successfully."
              : "PIN removed successfully.",
          message: "",
        },
      };

    case PIN_ENTERED:
      return {
        ...state,
        pinEnteredCorrect: true,
      };

    case FILE_SAVED:
      return {
        ...state,
        eventMessage: {
          type: action.success ? SUCCESS : ERROR,
          heading: action.heading,
          message: action.message,
        },
      };

    case ERROR:
      return {
        ...state,
        eventMessage: {
          type: ERROR,
          heading: action.heading,
          message: action.message,
        },
      };

    case ERROR_SHOWN:
      return {
        ...state,
        eventMessage: {
          type: null,
          heading: null,
          message: null,
        },
      };

    default:
      return state;
  }
};

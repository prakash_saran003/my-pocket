import { CREDIT, DEBIT } from "../Constants/constants";
import {
  getAllListData,
  getTotalAmount,
  insertAmount,
  addListItemToTable,
  getAll,
  insertBorrowedItem,
  getBorrowedItemsFromTable,
  getBorrowedItemByDesc,
  updateBorrowedItemByDesc,
  deleteTransactionFromTable,
  getSettings,
  updateSetting,
  getStatementBetweenDates,
  updateTransactionInTable,
  getTransactionById,
} from "../Database/db";

export const INIT = "INIT";
export const ADD_TRANSACTION = "ADD_TRANSACTION";
export const UPDATE_TRANSACTION = "UPDATE_TRANSACTION";
export const DELETE_TRANSACTION = "DELETE_TRANSACTION";
export const ADD_BORROWED_ITEM = "ADD_BORROWED_ITEM";
export const ADD_LIST_ITEM = "ADD_LIST_ITEM";
export const GET_STATEMENT = "GET_STATEMENT";
export const GET_BORROWED_ITEMS = "GET_BORROWED_ITEMS";
export const GET_BORROWED_ITEM = "GET_BORROWED_ITEM";
export const ERROR = "ERROR";
export const ERROR_SHOWN = "ERROR_SHOWN";
export const UPDATE_BORROWED_ITEM = "UPDATE_BORROWED_ITEM";
export const UPDATE_CURRENCY = "UPDATE_CURRENCY";
export const UPDATE_PIN = "UPDATE_PIN";
export const PIN_ENTERED = "PIN_ENTERED";
export const FILE_SAVED = "FILE_SAVED";

export const initState = () => {
  return async (dispatch) => {
    const date = new Date();
    var startOfMonth = new Date(date.getFullYear(), date.getMonth(), 1);
    startOfMonth = startOfMonth.getTime();

    let res = await getTotalAmount(CREDIT, +startOfMonth);
    const creditData = res.rows._array;
    let totalCredit = creditData[0].totalAmount;

    res = await getTotalAmount(DEBIT, +startOfMonth);
    const debitData = res.rows._array;
    let totalDebit = debitData[0].totalAmount;

    res = await getAllListData();
    const ld = res.rows._array;

    res = await getSettings();
    let objj = {};
    res.rows._array.forEach((rr) => {
      objj[rr.setting] = rr.value;
    });
    // console.log("actions objj : ", objj);

    dispatch({
      type: INIT,
      balance: +totalCredit - +totalDebit,
      income: +totalCredit,
      expense: +totalDebit,
      listData: ld,
      appPin: objj["app-pin"],
      currency: objj["currency"],
      email: objj["email"],
    });
  };
};

export const addTransaction = (data) => {
  console.log("addTransaction actions data : ", data);
  return async (dispatch) => {
    if (data.editTransaction && data.id) {
      console.log("IFFF");
      let prevValues = await getTransactionById(data.id);
      prevValues = prevValues.rows._array[0];
      // console.log("addTransaction prevValues : ", prevValues);
      await updateTransactionInTable(
        data.id,
        data.amount,
        data.description,
        data.type,
        data.date,
        data.moneyFor,
        data.paymentMode
      );

      dispatch({
        type: UPDATE_TRANSACTION,
        prevValues: prevValues,
        data: {
          amount: +data.amount,
          date: data.date,
          description: data.description,
          forBorrowedItem: data.forBorrowedItem ? data.forBorrowedItem : 0,
          id: data.id,
          moneyFor: data.moneyFor,
          paymentMode: data.paymentMode,
          status: 1,
          transactionType: data.type,
        },
      });
    } else {
      console.log("ELSE");

      await insertAmount(
        data.amount,
        data.description,
        data.type,
        data.date,
        data.moneyFor,
        data.paymentMode
      );
      dispatch({
        type: ADD_TRANSACTION,
        amount: +data.amount,
        transactionType: data.type,
      });
    }
  };
};

export const addListItem = (data) => {
  console.log("LIST ITEM TO ADD  : ", data);
  return (dispatch) => {
    addListItemToTable(data.item, data.itemType)
      .then((res) => {
        console.log("LIST ADDDED TO TABLE  : ", res);
        dispatch({
          type: ADD_LIST_ITEM,
          id: res.insertId,
          ...data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

export const getStatement = (startDate, endDate) => {
  // console.log("startDate : ", startDate, " endDate : ", endDate);
  return (dispatch) => {
    getStatementBetweenDates(startDate, endDate).then((res) => {
      // console.log("getStatementBetweenDates : ", res.rows._array);
      res.rows._array.sort((a, b) => {
        if (a.date > b.date) return -1;
        else if (a.date < b.date) return 1;
        else {
          if (a.id > b.id) return -1;
          else if (a.id < b.id) return 1;
          else return 0;
        }
      });

      dispatch({
        type: GET_STATEMENT,
        data: res.rows._array,
      });
    });
  };
};

export const getBorrowedItems = () => {
  return (dispatch) => {
    getBorrowedItemsFromTable().then((res) => {
      let dt = res.rows._array;
      // console.log("GET BORROWED ITEMS L:::", dt);
      let itemObj = {};
      dt.forEach((element) => {
        if (element.description in itemObj) {
          itemObj[element.description].push(element);
        } else {
          itemObj[element.description] = [element];
        }
      });
      Object.keys(itemObj).forEach((key) => {
        itemObj[key] = itemObj[key].sort((a, b) => {
          if (a.date > b.date) return -1;
          else if (a.date < b.date) return 1;
          else {
            if (a.id > b.id) return -1;
            else if (a.id < b.id) return 1;
            else return 0;
          }
        });
      });
      dispatch({
        type: GET_BORROWED_ITEMS,
        data: itemObj,
      });
    });
  };
};

export const addBorrowedItem = (data) => {
  console.log("data ACTIONS addBorrowedItem: ", data);
  return async (dispatch) => {
    if (data.update) {
      console.log("IF======>>");
      let amountInsertId = null;
      let resInsert;
      let expenseToAdd = 0;

      if (data.transactionId && data.transactionId !== "null") {
        if (!data.paid) {
          resInsert = await deleteTransactionFromTable(data.transactionId);
          // console.log("deleteTransactionFromTable : ", resInsert);
          amountInsertId = null;
          expenseToAdd = -1 * +data.amount;
        } else if (data.paid && data.paid === 1) {
          let prevValues = await getTransactionById(data.transactionId);
          prevValues = prevValues.rows._array[0];
          // console.log("getTransactionById : ", prevValues);

          resInsert = await updateTransactionInTable(
            data.transactionId,
            data.amount,
            data.description,
            data.type,
            data.date,
            data.description,
            data.paymentMode,
            true
          );
          // console.log("updateTransactionInTable : ", resInsert);
          amountInsertId = data.transactionId;

          expenseToAdd = -1 * +prevValues.amount;
        }
      } else {
        if (data.paid && data.paid === 1) {
          resInsert = await insertAmount(
            data.amount,
            data.description,
            data.type,
            data.date,
            data.description,
            data.paymentMode,
            true
          );
          // console.log("insertAmount : ", resInsert);
          amountInsertId = resInsert.insertId;
        }
      }
      let res = await updateBorrowedItemByDesc(
        data.id,
        data.description.trim(),
        data.quantity,
        data.paid,
        data.amount,
        data.paymentMode,
        data.date,
        amountInsertId
      );
      // console.log("updateBorrowedItemByDesc : ", res);

      dispatch({
        type: UPDATE_BORROWED_ITEM,
        data: {
          id: data.id,
          description: data.description.trim(),
          quantity: +data.quantity,
          paid: data.paid ? 1 : 0,
          amount: +data.amount,
          paymentMode: data.paymentMode,
          date: +data.date,
          transactionType: data.type,
          transactionId: amountInsertId,
          expenseToAdd: expenseToAdd,
        },
      });
    } else {
      let res = await getBorrowedItemByDesc(data.description.trim());

      if (res.rows.length > 0 && data.checkForDuplicate) {
        // console.log("RES.LENGTH : ", res.rows.length);
        return dispatch({
          type: ERROR,
          heading: "Duplicate Borrowed Item",
          message: "This Borrowed Item is already present!",
        });
      } else {
        console.log("ELSE======>>");
        let amountInsertId = null;
        let resInsert;
        if (data.paid && data.paid === 1) {
          resInsert = await insertAmount(
            data.amount,
            data.description,
            data.type,
            data.date,
            data.description,
            data.paymentMode,
            true
          );
          console.log("insertAmount: ", resInsert);
          amountInsertId = resInsert.insertId;
        }

        let result = await insertBorrowedItem(
          data.description.trim(),
          +data.quantity,
          data.paid,
          +data.amount,
          data.paymentMode,
          +data.date,
          amountInsertId
        );

        // .then((result) => {
        console.log("result THEN actions : ", result);
        dispatch({
          type: ADD_BORROWED_ITEM,
          data: {
            id: result.insertId,
            description: data.description.trim(),
            quantity: +data.quantity,
            paid: data.paid ? 1 : 0,
            amount: +data.amount,
            paymentMode: data.paymentMode,
            date: +data.date,
            transactionId: amountInsertId,
            transactionType: data.type,
          },
        });
        // })
        // .catch((err) => {
        //   console.log(err);
        // });
      }
      // });
    }
    // .catch((err) => {
    //   console.log(err);
    // });
  };
};

export const eventMessageShown = () => {
  return {
    type: ERROR_SHOWN,
  };
};

export const deleteTransaction = (data) => (dispatch) => {
  console.log("deleteTransaction : ", data);

  deleteTransactionFromTable(data.id).then((res) => {
    // console.log("deleteTransactionFromTable : ", res);
    dispatch({
      type: DELETE_TRANSACTION,
      data: data,
    });
  });
};

export const updateCurrency = (code) => (dispatch) => {
  updateSetting("currency", code).then((res) => {
    dispatch({
      type: UPDATE_CURRENCY,
      code: code,
    });
  });
};

export const updatePin = (data) => {
  return async function (dispatch) {
    console.log("updatePin : ", data);

    const pinUpdated = await updateSetting("app-pin", data.pin);
    console.log("pinUpdated : ", pinUpdated);

    const emailUpdated = await updateSetting("email", data.email);
    console.log("emailUpdated : ", emailUpdated);

    // const fingerLockUpdated = await updateSetting(
    //   "fingerLock",
    //   data.fingerLock
    // );
    // console.log("fingerLockUpdated : ", fingerLockUpdated);

    dispatch({
      type: UPDATE_PIN,
      email: data.email,
      pin: data.pin,
      // fingerLock: data.fingerLock,
    });
  };
};

export const pinEnteredCorrectFunc = () => {
  return {
    type: PIN_ENTERED,
  };
};

export const fileSaved = (data) => {
  return {
    type: FILE_SAVED,
    ...data,
  };
};

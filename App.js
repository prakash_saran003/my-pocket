import React, { useState } from "react";
import { NativeBaseProvider, extendTheme, Text } from "native-base";
import { StyleSheet } from "react-native";
import { Provider } from "react-redux";
import { applyMiddleware, createStore } from "redux";
import ReduxThunk from "redux-thunk";

import config from "./nativebase.config";
import RootComponent from "./Components/RootComponent";
import MainReducer from "./Store/reducer";
import {
  addMultipleListData,
  getAll,
  getAllListData,
  getAllTables,
  initDb,
  initSettings,
} from "./Database/db";
import { expenseList, incomeList, paymentList } from "./Constants/listItem";

getAllTables().then((res) => {
  console.log("ALL TABLES : ", res.rows._array);
  getAll().then((res) => {
    // console.log("TRANSACTION TABLE : ", res.rows._array);
  });
});

const store = createStore(MainReducer, applyMiddleware(ReduxThunk));

export default function App() {
  const [tablesCreated, setTablesCreated] = useState(false);

  const theme = extendTheme({
    components: {
      Text: {
        defaultProps: {
          fontSize: "lg",
        },
      },
    },
    colors: {
      slateGray: {
        50: "#f3f2f2",
        100: "#d8d8d8",
        200: "#bebebe",
        300: "#a3a3a3",
        400: "#898989",
        500: "#6f6f6f",
        600: "#565656",
        700: "#3e3e3e",
        800: "#252525",
        900: "#0d0c0d",
      },
    },
    Pressable: {
      cursor: "pointer",
    },

    config: {
      // Changing initialColorMode to 'dark'
      initialColorMode: "dark",
      // useSystemColorMode: true,
    },
  });
  let rootComp = <RootComponent />;
  if (!tablesCreated) {
    initDb()
      .then(() => {
        console.log("Initialized db");

        getAllListData().then((res) => {
          // console.log("LIST DATAATT : ", res.rows);
          if (res.rows.length === 0) {
            addMultipleListData(paymentList, "payment-mode");
            addMultipleListData(incomeList, "income-mode");
            addMultipleListData(expenseList, "expense-mode");
            initSettings();
          }
        });
        setTablesCreated(true);
      })
      .catch((err) => {
        console.log("Initialized db failed.", err);
      });
    rootComp = <Text>CREATING TABLE</Text>;
  }

  return (
    <Provider store={store}>
      <NativeBaseProvider theme={theme} config={config}>
        {rootComp}
      </NativeBaseProvider>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});

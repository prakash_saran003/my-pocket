import React, { useState } from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { useColorModeValue, useTheme } from "native-base";

import { Home } from "../screens/Home";
import DailyExpense from "../screens/DailyExpense";
import BorrowedItemsList from "../screens/BorrowedItemsList";
import AddNewBorrowedItem from "../screens/AddNewBorrowedItem";
import Statement from "../screens/Statement";
import BorrowedItemList from "../screens/BorrowedItemList";
import Settings from "../screens/Settings";
import Currency from "../screens/Currency";
import PasswordScreen from "../screens/PasswordScreen";
import LockScreen from "../screens/LockScreen";
import AppLoading from "expo-app-loading";
import { useDispatch, useSelector } from "react-redux";
import { initState } from "../Store/actions";
import PopUpMenu from "../Components/PopUpMenu";

const Stack = createNativeStackNavigator();

export function RootNavigator() {
  const { colors } = useTheme();
  //   console.log('COLORS : ',colors);
  const colorMode = useColorModeValue("light", "dark");
  // console.log("colorMode : ", colorMode);

  const defaultOption = {
    headerShown: true,
    headerStyle: {
      backgroundColor:
        colorMode === "dark"
          ? colors["blueGray"]["900"]
          : colors["blueGray"]["100"],
    },
    headerTitleStyle: {
      fontWeight: "100",
    },
    headerTintColor:
      colorMode === "dark" ? colors["lightText"] : colors["darkText"],
  };

  const [isReady, setIsReady] = useState(false);
  const appPin = useSelector((state) => state.appPin);
  const pinEnteredCorrect = useSelector((state) => state.pinEnteredCorrect);
  const statement = useSelector((state) => state.statement);

  const dispatch = useDispatch();

  async function loadData() {
    await dispatch(initState());
  }

  if (!isReady) {
    return (
      <AppLoading
        startAsync={loadData}
        onFinish={() => {
          setIsReady(true);
        }}
        onError={console.warn}
      />
    );
  }

  // console.log("isReady: ", isReady);
  // console.log("root Nav appPin: ", appPin);
  // console.log("root Nav pinEnteredCorrect: ", pinEnteredCorrect);

  return (
    <Stack.Navigator
      initialRouteName={
        isReady && ((!!appPin && pinEnteredCorrect) || !appPin)
          ? "Home"
          : "LockScreen"
      }
    >
      {isReady && ((!!appPin && pinEnteredCorrect) || !appPin) ? (
        <>
          <Stack.Screen
            name="Home"
            component={Home}
            options={{
              ...defaultOption,
              headerTitle: "Expense Tracker",
            }}
          />
          <Stack.Screen
            name="DailyExpense"
            component={DailyExpense}
            options={{
              ...defaultOption,
              headerTitle: "Add Daily Expense",
            }}
          />
          <Stack.Screen
            name="BorrowedItems"
            component={BorrowedItemsList}
            options={{
              ...defaultOption,
              headerTitle: "Borrowed Items",
            }}
          />
          <Stack.Screen
            name="AddNewBorrowedItem"
            component={AddNewBorrowedItem}
            options={{
              ...defaultOption,
              headerTitle: "Add New Borrowed Item",
            }}
          />
          <Stack.Screen
            name="Statement"
            component={Statement}
            options={{
              ...defaultOption,
              headerTitle: "All Transactions",
              headerRight: () => (statement.length > 0 ? <PopUpMenu /> : null),
            }}
          />
          <Stack.Screen
            name="Settings"
            component={Settings}
            options={{
              ...defaultOption,
              headerTitle: "Settings",
            }}
          />
          <Stack.Screen
            name="BorrowedItemList"
            component={BorrowedItemList}
            // options={{
            //   ...defaultOption,
            //   headerTitle: "Borrowed Item Statement",
            // }}
            options={({ route }) => ({
              ...defaultOption,
              headerTitle: route.params.itemName,
            })}
          />
          <Stack.Screen
            name="Currency"
            component={Currency}
            options={{
              ...defaultOption,
              headerTitle: "Set Currency",
            }}
          />
          <Stack.Screen
            name="PasswordScreen"
            component={PasswordScreen}
            options={{
              ...defaultOption,
              headerTitle: "Set PIN Lock",
            }}
          />
        </>
      ) : (
        <Stack.Screen
          name="LockScreen"
          component={LockScreen}
          options={{
            headerShown: false,
          }}
        />
      )}
    </Stack.Navigator>
  );
}
